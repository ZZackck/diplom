package programm.FieldsJson;

import programm.Interfaces.IField;
import programm.Utilites.JsonMap;

/**
 * Created by kir on 17.10.16.
 */
public class FieldAttrs implements IField {
    private String name;
    private String type;
    private String flexberryType;
    private boolean notNull;
    private String defaultValue;

    public FieldAttrs() {

    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getFlexberryType() {
        return flexberryType;
    }

    public boolean isNotNull() {
        return notNull;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFlexberryType(String flexberryType) {
        this.flexberryType = flexberryType;
    }

    public void setNotNull(boolean notNull) { this.notNull = notNull; }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Создание аннтоационных полей
     * @return
     */
    public String getAnnotatedFiled() {
        String result = "%s\t@Column(name=" + '"' + name + '"' +
                ", nullable=" + !notNull + "%s)\n" +
                "\tprivate %s %s;\n";
        String type;
        String columnLength = "";
        String modifier = "";
        if (flexberryType.contains("nvarchar")) {
            columnLength += ", length=" + flexberryType.substring(8, flexberryType.length());
        } else if (flexberryType.contains("nchar")) {
            columnLength += ", length=" + flexberryType.substring(5, flexberryType.length());
        } else if (this.type.contains("date")) {
            modifier = " \t@Temporal(TemporalType.DATE)\n";
        }

        type = javaType();
        return String.format(result, modifier, columnLength, type, this.name) + '\n';
    }

    /**
     * Строковый паттерн для формирования геттеров классов
     * @return
     */
    public String getGetter() {
        String result = "\tpublic %s %s() {\n" +
                "\t\treturn this.%s;\n" +
                "\t}\n";
        String getterName = "get" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        String type = javaType();
        return String.format(result, type, getterName, name);
    }

    /**
     * Строковый паттерн для формирования сеттеров классов
     * @return
     */
    public String getSetter() {
        String result = "\tpublic void %s(%s %s) {\n" +
                "\t\tthis.%s = %s;\n" +
                "\t}\n";
        String setterName = "set" + Character.toUpperCase(name.charAt(0)) + name.substring(1);
        String type = javaType();
        return String.format(result, setterName, type, name, name, name);
    }

    /**
     * Проверка на необходимость импорта
     * @return
     */
    public Boolean hasImport() {
        switch (type) {
            case "date":
                return true;
            case "decimal":
                return true;
            case "number":
                return true;
            default:
                return false;
        }
    }

    /**
     * Импорт дополнительных классовых библиотек
     * @return
     */
    public String getImport() {
        switch (type) {
            case "date":
                return "import java.util.Date;";
            case "decimal":
                return "import java.math.BigDecimal;";
            case "number":
                return "import java.math.BigDecimal;";
            default:
                return "";
        }
    }

    /**
     * Установление соответствий типов между JSON и DB MySQL полями
     * @return
     */
    public String javaType() {
        if (this.flexberryType.contains("nchar") || this.flexberryType.contains("nvarchar"))
            return JsonMap.getJavaByFlexberry(this.type);
        else
            return JsonMap.getJavaByFlexberry(this.flexberryType);
    }
}