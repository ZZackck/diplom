package programm.FieldsJson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kir on 17.10.16.
 */
public class FieldProjections {
    private String name;
    private String modelName ;
    private List<Object>attrs;
    private List<Object>belongsTo;
    private List<Object>hasMany;

    public FieldProjections() {
        name = "";
        modelName = "";
        attrs = new ArrayList<Object>();
        belongsTo = new ArrayList<Object>();
        hasMany = new ArrayList<Object>();
    }

    public String getName() {
        return name;
    }

    public String getModelName() {
        return modelName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public void setAttrs(List<Object> attrs) {
        this.attrs = attrs;
    }

    public void setBelongsTo(List<Object> belongsTo) { this.belongsTo = belongsTo; }

    public void setHasMany(List<Object> hasMany) {
        this.hasMany = hasMany;
    }

    public List<Object> getAttrs() {
        return attrs;
    }

    public List<Object> getBelongsTo() {
        return belongsTo;
    }

    public List<Object> getHasMany() {
        return hasMany;
    }
}
