package programm.FieldsJson;

import programm.Interfaces.IField;

/**
 * Created by kir on 17.10.16.
 */
public class FieldBelogsTo implements IField {
    private String startMultiplicity;
    private String name;
    private String relatedTo;
    private String inverse;

    public FieldBelogsTo() {

    }

    public String getStartMultiplicity() {
        return startMultiplicity;
    }

    public String getName() {
        return name;
    }

    public String getRelatedTo() { return relatedTo; }

    public String getInverse() {
        return inverse;
    }

    public void setStartMultiplicity(String startMultiplicity) {
        this.startMultiplicity = startMultiplicity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRelatedTo(String relatedTo) {
        this.relatedTo = relatedTo;
    }

    public void setInverse(String inverse) {
        this.inverse = inverse;
    }

    /**
     * Создание аннотационных полей
     * @return
     */
    public String getAnnotatedFiled() {
        String result = "\t@ManyToOne(fetch = FetchType.LAZY)\n" +
                "\t@JoinColumn(name = \"%s\")\n" +
                "\tprivate %s %s;\n";
        String related = relatedTo.replaceAll("-","");
        return String.format(result, this.name,related,this.name);
    }

    /**
     * Строковый паттерн для формирования геттеров классов
     * @return
     */
    public String getGetter() {
        String result = "\tpublic %s get%s() {\n" +
                "\t\treturn %s;\n" +
                "\t}\n";
        String nm = Character.toUpperCase(this.name.charAt(0))+this.name.substring(1);
        String related = relatedTo.replaceAll("-","");
        return String.format(result, related,nm,this.name);
    }

    /**
     * Строковый паттерн для формирования сеттеров классов
     * @return
     */
    public String getSetter() {
        String res = "\tpublic void set%s(%s %s) {\n" +
                "\t\tthis.%s = %s;\n" +
                "\t}\n";
        String nm = Character.toUpperCase(this.name.charAt(0))+this.name.substring(1);
        String related = relatedTo.replaceAll("-","");
        return String.format(res, nm,related,this.name,this.name,this.name);
    }
}
