package programm.FieldsJson;

import programm.Interfaces.IField;

/**
 * Created by kir on 17.10.16.
 */
public class FieldHasMany implements IField {
    private String name;
    private String relatedTo;
    private String inverse;

    public FieldHasMany() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public String getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(String relatedTo) {
        this.relatedTo = relatedTo;
    }

    public String getInverse() {
        return inverse;
    }

    public void setInverse(String inverse) {
        this.inverse = inverse;
    }

    /**
     * Создание аннотационных полей
     * @return
     */
    public String getAnnotatedFiled() {
        String result = "\t@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)\n" +
                "\t@JoinColumn(name = \"%s\")\n" +
                "\tprivate List<%s> %s = new ArrayList<%s>();\n";
        String related = relatedTo.replaceAll("-","");
        return String.format(result, name,related,name,related);
    }

    /**
     * Строковый паттерн для формирования геттеров классов
     * @return
     */
    public String getGetter() {
        String result = "\tpublic List<%s> get%s() {\n" +
                "\t\treturn %s;\n" +
                "\t}\n";
        String nm = Character.toUpperCase(name.charAt(0))+name.substring(1);
        String related = relatedTo.replaceAll("-","");
        return String.format(result, related,nm,name);
    }

    /**
     * Строковый паттерн для формирования сеттеров классов
     * @return
     */
    public String getSetter() {
        String related = relatedTo.replaceAll("-","");
        String res = "\tpublic void set%s(%s %s) {\n" +
                "\t\tthis.%s = (List<"+related+">) %s;\n" +
                "\t}\n";
        String nm = Character.toUpperCase(name.charAt(0))+name.substring(1);
        return String.format(res, nm,related,name,name,name);
    }

    public boolean hasImport() {
        return true;
    }

    /**
     * Импорт дополнительных классовых библиотек
     * @return
     */
    public String getImport() {
        String imp = new String();
        imp += "import java.util.ArrayList;\n";
        imp += "import java.util.List;";
        return imp;
    }
}
