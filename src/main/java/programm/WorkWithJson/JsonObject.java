package programm.WorkWithJson;

import programm.FieldsJson.*;
import programm.Interfaces.IField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kir on 16.10.16.
 */
public class JsonObject {
    private String name;
    private String pckg;
    private List<FieldAttrs>attrs;
    private List<FieldBelogsTo>belongsTo;
    private List<FieldHasMany>hasMany;
    private List<FieldProjections>projections;

    public JsonObject(){
        name = "";
        pckg = "";
        attrs = new ArrayList<FieldAttrs>();
        belongsTo = new ArrayList<FieldBelogsTo>();
        hasMany = new ArrayList<FieldHasMany>();
        projections = new ArrayList<FieldProjections>();
    }

    public List<FieldAttrs> getAttrs() {
        return attrs;
    }

    public List<FieldBelogsTo> getBelongsTo() {
        return belongsTo;
    }

    public List<FieldHasMany> getHasMany() {
        return hasMany;
    }

    public List<FieldProjections> getProjections() {
        return projections;
    }

    public void setAttrs(List<FieldAttrs> attrs) {
        this.attrs = attrs;
    }

    public void setBelongsTo(List<FieldBelogsTo> belongsTo) {
        this.belongsTo = belongsTo;
    }

    public void setHasMany(List<FieldHasMany> hasMany) {
        this.hasMany = hasMany;
    }

    public void setProjections(List<FieldProjections> projections) {
        this.projections = projections;
    }

    public String getName() { return name.replaceAll("-",""); }

    public String getPckg() { return pckg; }

    public void setName(String name) {
        this.name = name;
    }

    public void setPckg(String pckg) {
        this.pckg = pckg;
    }

    /**
     * Преобразование JSON объектов в класс (строковый вид)
     * @return
     */
    public String getClassAsString() {
        String classPattern =
                "%s\n\n" +
                        "%s\n" +
                        "@Entity \n" +
                        "@Table(name=%s)\n" +
                        "public class %s {\n\n" +
                        "%s \n" +
                        "%s \n" +
                        "%s \n" +
                        "}\n";
        String stringField = "\t@Id\n" +
                "\t@Column(name = \"ID\")\n" +
                "\t@GeneratedValue(strategy=GenerationType.AUTO)\n" +
                "\tprivate Integer ID;\n\n";
        String stringGetters = "\tpublic Integer getId(){\n" +
                "\t\treturn ID;\n" +
                "\t}\n";
        String stringSetters = "\tpublic void setId (Integer ID){\n" +
                "\t\tthis.ID=ID;\n" +
                "\t}\n";
        String importLibs = "";

        List<IField> fields = getFields();

        for (IField field : fields) {
            if(field instanceof FieldAttrs) {
                FieldAttrs attr = (FieldAttrs) field;
                if(attr.hasImport() && !importLibs.contains(attr.getImport()))
                    importLibs += attr.getImport() + "\n";
            }

            if (field instanceof FieldHasMany) {
                FieldHasMany attr = (FieldHasMany) field;
                if(attr.hasImport())
                    importLibs += attr.getImport() + "\n";
            }
            stringField += field.getAnnotatedFiled();
            stringGetters += field.getGetter();
            stringSetters += field.getSetter();
        }

        importLibs += "import javax.persistence.*;\n";

        return String.format(classPattern,"package " + pckg+';', importLibs,
                '"' + getName() + '"', getName(), stringField, stringGetters, stringSetters);
    }

    /**
     * Добавление "полей" JSON обхектов в объект интерфейса IField
     * @return
     */
    private List<IField> getFields() {
        List<IField> fields = new ArrayList<IField>();
        fields.addAll(attrs);
        fields.addAll(belongsTo);
        fields.addAll(hasMany);
        return fields;
    }

    /**
     * Получение полного имени класса
     * @return
     */
    public String getFullName() {
        return pckg+'.'+getName();
    }
}
