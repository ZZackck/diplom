package programm;

import programm.CreateConfigFiles.*;
import programm.HibernateConfig.HibernateConfigGenerator;
import programm.Parser.ParserClass;
import programm.Utilites.BuildAllConfigFiles;
import programm.Utilites.ClassWriter;
import programm.Utilites.Constants;
import programm.WorkWithJson.JsonObject;
import programm.WorkWithShell.DoShell;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kir on 15.10.16.
 */
public class Main {
    public static void main(String[] args) {
        List<JsonObject> jsonObjectList = new ArrayList<JsonObject>();
        if (true) {
            File inputFolder = new File(Constants.JSON_FILES_SOURCE);
            System.out.println("Generating java object from json:");
            for (File file : inputFolder.listFiles()) {
                if (file.isFile() && file.getName().endsWith(".json")) {
                    jsonObjectList.add(new ParserClass().beginParse(file));
                }
            }
            System.out.println("Generating java object from json: DONE\n");
        }

        HibernateConfigGenerator hibCfgGen = new HibernateConfigGenerator();
        System.out.println("Generating java classes:");
        for (JsonObject object : jsonObjectList) {
            ClassWriter.writeClass(object);
            hibCfgGen.addObject(object);
        }
        System.out.println("Generating java classes:DONE\n");
        System.out.println("Generating other configuration file:");


        BuildAllConfigFiles buildAllConfigFiles = new BuildAllConfigFiles(hibCfgGen);

        if(buildAllConfigFiles.buildFiles(jsonObjectList.get(0).getPckg())) {
            System.out.println("Generating other files:DONE!\n");
        }
        else
        {
            System.out.println("Generating other files:FAIL!\n");
        }
    }
}