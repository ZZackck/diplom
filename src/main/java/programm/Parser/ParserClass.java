package programm.Parser;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import programm.WorkWithJson.JsonObject;

import java.io.*;

/**
 * Created by kir on 17.10.16.
 */
public class ParserClass {

    public ParserClass() {

    }

    /**
     * Считываение данных из JSON-файла
     * @param file
     * @return
     */
    public JsonObject beginParse(File file){
        JsonObject objectJson = new JsonObject();

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            fileInputStream.read(data);
            fileInputStream.close();
            String dataString = new String(data, "UTF-8");
            objectJson = parse(dataString.substring(1));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return objectJson;
    }

    /**
     * парсинг JSON-файла через ObjectMapper
     * @param json
     * @return
     */
    private JsonObject parse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        JsonObject jsonObject = new JsonObject();

        try {
            jsonObject = mapper.readValue(json,JsonObject.class);
            System.out.println("Java object created from JSON String: " + jsonObject.getName());
        } catch (JsonGenerationException ex) {
            ex.printStackTrace();
        } catch (JsonMappingException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return jsonObject;
    }
}
