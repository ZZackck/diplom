package programm.HibernateConfig;

import programm.Utilites.Constants;
import programm.WorkWithJson.JsonObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kir on 21.11.16.
 */
public class HibernateConfigGenerator {
    private static final String FILE_NAME = "hibernate.cfg.xml";
    private static final String MAPPING_CLASS_PATTERN = "<mapping class=\"%s\"/>";
    private List<JsonObject> jsonObjects = new ArrayList<JsonObject>();

    private String hibernatePattern = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
            "<!DOCTYPE hibernate-configuration PUBLIC\n"+
            "\t\"-//Hibernate/Hibernate Configuration DTD//EN\"\n"+
            "\t\"http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd\">\n"+
            "<hibernate-configuration>\n"+
            "<session-factory>\n"+
                "\t<property name=\"hibernate.connection.driver_class\">com.mysql.jdbc.Driver</property>\n"+
                "\t<property name=\"hibernate.connection.password\">3626</property>\n"+
                "\t<property name=\"hibernate.connection.url\">jdbc:mysql://localhost:3306/javadb?createDatabaseIfNotExist=true</property>\n"+
                "\t<property name=\"hibernate.connection.username\">root</property>\n"+
                "\t<property name=\"hibernate.dialect\">org.hibernate.dialect.MySQLDialect</property>\n"+
                "\t<property name=\"hibernate.hbm2ddl.auto\">update</property>\n"+
                "\t<property name=\"show_sql\">true</property>\n"+
                "\t<property name=\"connection_poolsize\">1</property>\n"+
                "\n"+
                "%s"+
            "</session-factory>\n"+
            "</hibernate-configuration>\n";

    public HibernateConfigGenerator() {
    }

    /**
     * Добавлене Json объектов
     * @param object
     */
    public void addObject(JsonObject object) {
        jsonObjects.add(object);
    }

    /**
     * Генерация гибернейт файла
     */
    public void generateFile() {
        String mappings = classsMappingToString();
        String fileContent = String.format(hibernatePattern, mappings);
        File hiberFile = createFile();
        try {
            FileWriter writer = new FileWriter(hiberFile);
            writer.write(fileContent);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Добавление полного названия классов в паттерн файла гибернейт
     * @return
     */
    private String classsMappingToString() {
        String mappings = "";
        for(JsonObject object:jsonObjects){
            mappings+= "\t"+String.format(MAPPING_CLASS_PATTERN, object.getFullName())+'\n';
        }
        return mappings;
    }

    /**
     * Создание файла
     * @return
     */
    private File createFile() {
        File resourceFolder = new File(Constants.RESOURCES_FOLDER);
        if(!resourceFolder.exists())
            resourceFolder.mkdirs();

        File hiberFile = new File(Constants.RESOURCES_FOLDER+'/'+FILE_NAME);
        if(!hiberFile.exists())
            try {
                hiberFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        return hiberFile;
    }
}
