package programm.Utilites;

import programm.WorkWithJson.JsonObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by kir on 07.11.16.
 */
public class ClassWriter {
    public ClassWriter() {
    }

    /**
     * Записать аннотационные Persistence классы, основванные на метаданных
     * @param obj
     */
    public static void writeClass(JsonObject obj) {
        String name = obj.getName();
        File file = new File(Constants.CLASS_SOURCE+'/'+obj.getPckg());
        if(!file.exists()) {
            file.mkdirs();
        }
        file = new File(file.getPath()+"/"+name+".java");
        if(file.exists())
            file.delete();
        try {
            System.out.println(file.getAbsolutePath());
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(obj.getClassAsString());
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
