package programm.Utilites;

import programm.CreateConfigFiles.*;
import programm.HibernateConfig.HibernateConfigGenerator;
import programm.Interfaces.IFilePattern;
import programm.WorkWithJson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kir on 11.05.17.
 */
public class BuildAllConfigFiles {
    private HibernateConfigGenerator hibernateConfigGenerator;


    public BuildAllConfigFiles(HibernateConfigGenerator hibCfgGen) {
        hibernateConfigGenerator = hibCfgGen;
    }

    /**
     * Создание файлов
     * @return
     */
    public boolean buildFiles(String pkg) {
        try {
            hibernateConfigGenerator.generateFile();
            BuildFile buildFile = new BuildFile();
            IFilePattern.class.getClasses();
            List<IFilePattern> files = getFiles(pkg);

            for (IFilePattern iFilePattern: files) {
                buildFile.createFile(iFilePattern.getFileName(),iFilePattern.getFileStructure(),iFilePattern.getFileDestination());
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Добавление генерируемых конфигуационных файлов в экземпляр интерейса IFilePattern
     * @return
     */
    private List<IFilePattern> getFiles(String pkg) {
        List<IFilePattern> iFilePatterns = new ArrayList<>();
        iFilePatterns.add(new BuildClassNamesIFile(pkg));
        iFilePatterns.add(new BuildDemoEdmProviderIFile());
        iFilePatterns.add(new BuildDemoEntityCollectionProcessorIFile());
        iFilePatterns.add(new BuildEntityProcessorIFile());
        iFilePatterns.add(new BuildFilterExpressionVisitorIFile());
        iFilePatterns.add(new BuildHibernateFactoryIFile());
        iFilePatterns.add(new BuildHibernateQueryIFile());
        iFilePatterns.add(new BuildHibernateToEdmTypesIFile());
        iFilePatterns.add(new BuildPomIFile());
        iFilePatterns.add(new BuildPrettyPrintIFile());
        iFilePatterns.add(new BuildPrimitiveProcessorFile());
        iFilePatterns.add(new BuildServletIFile());
        iFilePatterns.add(new BuildShellIFile());
        iFilePatterns.add(new BuildStorageIFile());
        iFilePatterns.add(new BuildWebXMLIFile());
        iFilePatterns.add(new BuildUtilIFile());
        return iFilePatterns;
    }
}
