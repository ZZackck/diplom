package programm.Utilites;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kir on 19.11.16.
 */
public class JsonMap {
    private static Map<String, String> flexberryToJavaTypes;

    /**
     * Сопоставление типов данных метаданных и MySQL
     */
    static {
        flexberryToJavaTypes = new HashMap<String, String>();
        flexberryToJavaTypes.put("nvarchar", "String");
        flexberryToJavaTypes.put("string", "String");
        flexberryToJavaTypes.put("DateTime", "Date");
        flexberryToJavaTypes.put("money", "BigDecimal");
        flexberryToJavaTypes.put("short", "short");
        flexberryToJavaTypes.put("int", "int");
        flexberryToJavaTypes.put("float", "float");
        flexberryToJavaTypes.put("bool","boolean");
    }

    public static String getJavaByFlexberry(String flexberry) {
        return flexberryToJavaTypes.get(flexberry);
    }
}
