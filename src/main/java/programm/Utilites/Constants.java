package programm.Utilites;

/**
 * Created by kir on 20.10.16.
 */
public class Constants {
    public static final String JSON_FILES_SOURCE = "jsonfiles";
    public static final String CLASS_SOURCE = "GeneratedApplication/src/main/java";
    public static final String RESOURCES_FOLDER = "GeneratedApplication/src/main/resources";
    public static final String ROOT = "GeneratedApplication";
    public static final String WEBAPP = "GeneratedApplication/src/main/webapp/WEB-INF";
    public static final String SERVLET = "GeneratedApplication/src/main/java/servlet";
    public static final String ODATAUTILS = "GeneratedApplication/src/main/java/ODataUtils";
    public static final String HIBERNATEUTILS = "GeneratedApplication/src/main/java/model/HibernateUtils";
    public static final String SIMPLEUTILS = "GeneratedApplication/src/main/java/model/Utils";
}
