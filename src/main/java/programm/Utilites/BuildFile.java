package programm.Utilites;

import programm.Utilites.Constants;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by kir on 11.12.16.
 */
public class BuildFile {

    /**
     * Создание файла
     * @param fileName
     * @param pattern
     * @param dest
     */
    public void createFile(String fileName, String pattern, String dest) {
        File file = determineSource(dest);
        if(!file.exists()) {
            file.mkdirs();
        }
        file = new File(file.getPath()+"/"+fileName);
        if(file.exists())
            file.delete();
        try {
            System.out.println(file.getAbsolutePath());
            file.createNewFile();
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(pattern);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public BuildFile() {
    }

    /**
     * Создание файла по указанному пути
     * @param fileName
     * @return
     */
    private File determineSource(String fileName) {
       return new File(fileName);
    }
}
