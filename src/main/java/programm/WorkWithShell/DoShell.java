package programm.WorkWithShell;

/**
 * Created by kir on 11.12.16.
 */
public class DoShell {

    public DoShell() {
    }

    public void executeShell() {
        try {
            String cmd[] = {"gnome-terminal", "-x", "bash", "-c", "cd ./GeneratedApplication; chmod 777 Build.sh; sh Build.sh"};
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
