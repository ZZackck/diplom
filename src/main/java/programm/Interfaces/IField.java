package programm.Interfaces;

/**
 * Created by kir on 07.11.16.
 */
public interface IField {
    /**
     * Получение полей
     * @return
     */
    String getAnnotatedFiled();

    /**
     * Получение геттеров
     * @return
     */
    String getGetter();

    /**
     * Получение сеттеров
     * @return
     */
    String getSetter();
}
