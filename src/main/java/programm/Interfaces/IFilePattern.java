package programm.Interfaces;

/**
 * Created by kir on 11.05.17.
 */
public interface IFilePattern {
     /**
      * Получение названия файла
      * @return
      */
     String getFileName();

     /**
      * Получение структуры файла
      * @return
      */
     String getFileStructure();

     /**
      * Получение расположения файла
      * @return
      */
     String getFileDestination();
}
