package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;
import programm.WorkWithJson.JsonObject;

import java.util.List;

/**
 * Created by kir on 11.12.16.
 */
public class BuildClassNamesIFile implements IFilePattern {
    private String pkgName;
    private String fileName = "ClassNames.java";

    private String mainPattern = "package model.Utils;\n" +
            "\n" +
            "import org.reflections.Reflections;\n" +
            "\n" +
            "import java.util.ArrayList;\n" +
            "import java.util.List;\n" +
            "import java.util.Set;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 18.05.17.\n" +
            " */\n" +
            "public class ClassNames {\n" +
            "    private static List<String> names;\n" +
            "\n" +
            "    /**\n" +
            "     * Констурктор, получение аннотационных классов Java Persistence из пакета model\n" +
            "     */\n" +
            "    public ClassNames() {\n" +
            "        names = new ArrayList<>();\n" +
            "        Reflections reflections = new Reflections(\"%s\");\n" +
            "        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(javax.persistence.Entity.class);\n" +
            "\n" +
            "        for (Class<?> clazz : annotated)\n" +
            "            names.add(clazz.getSimpleName());\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Выдача списка заваний классов\n" +
            "     * @return\n" +
            "     */\n" +
            "    public static List<String> getNames() {\n" +
            "        return names;\n" +
            "    }\n" +
            "\n" +
            "    public static String containerName() {\n" +
            "        return \"%s\";\n" +
            "    }\n" +
            "}";

    public BuildClassNamesIFile(String pkg) {
        pkgName = pkg;
        setPackage();
    }

    private void setPackage() {
        mainPattern = String.format(mainPattern,pkgName,pkgName);
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return mainPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.SIMPLEUTILS;
    }
}
