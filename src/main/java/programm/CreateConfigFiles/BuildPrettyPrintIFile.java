package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 14.05.17.
 */
public class BuildPrettyPrintIFile implements IFilePattern{
    private String fileName = "PrettyPrintClass.java";

    private String fileStructure = "package model.Utils;\n" +
            "\n" +
            "import com.google.gson.Gson;\n" +
            "import com.google.gson.GsonBuilder;\n" +
            "import com.google.gson.JsonElement;\n" +
            "import com.google.gson.JsonParser;\n" +
            "import org.apache.commons.io.IOUtils;\n" +
            "\n" +
            "import java.io.ByteArrayInputStream;\n" +
            "import java.io.IOException;\n" +
            "import java.io.InputStream;\n" +
            "import java.io.StringWriter;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 13.05.17.\n" +
            " */\n" +
            "public class PrettyPrintClass {\n" +
            "\n" +
            "    /**\n" +
            "     * Крсивый вывод в JSON формате\n" +
            "     * @param in\n" +
            "     * @return\n" +
            "     */\n" +
            "    public static InputStream prettyPrint(InputStream in) {\n" +
            "        StringWriter writer = new StringWriter();\n" +
            "        try {\n" +
            "            IOUtils.copy(in, writer, \"UTF-8\");\n" +
            "        } catch (IOException e) {\n" +
            "            e.printStackTrace();\n" +
            "        }\n" +
            "        Gson gson = new GsonBuilder().setPrettyPrinting().create();\n" +
            "        JsonParser jp = new JsonParser();\n" +
            "        JsonElement je = jp.parse(writer.toString());\n" +
            "        String prettyJsonString = gson.toJson(je);\n" +
            "        return new ByteArrayInputStream(prettyJsonString.getBytes());\n" +
            "    }\n" +
            "}";

    public BuildPrettyPrintIFile() {
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return fileStructure;
    }

    @Override
    public String getFileDestination() {
        return Constants.SIMPLEUTILS;
    }
}
