package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 11.05.17.
 */
public class BuildHibernateToEdmTypesIFile implements IFilePattern {
    private String fileName = "HibernateToEdmTypesMap.java";
    private String fileStructure = "package model.HibernateUtils;\n" +
            "\n" +
            "import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeKind;\n" +
            "import org.apache.olingo.commons.api.edm.FullQualifiedName;\n" +
            "\n" +
            "import java.util.HashMap;\n" +
            "import java.util.Map;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 11.05.17.\n" +
            " */\n" +
            "public class HibernateToEdmTypesMap {\n" +
            "    private static Map<String, EdmPrimitiveTypeKind> edmTypes;\n" +
            "\n" +
            "    /**\n" +
            "     * Преобразование типов из MySQL в типы OData\n" +
            "     */\n" +
            "    static {\n" +
            "        edmTypes = new HashMap<String, EdmPrimitiveTypeKind>();\n" +
            "        edmTypes.put(\"varchar\", EdmPrimitiveTypeKind.String);\n" +
            "        edmTypes.put(\"date\", EdmPrimitiveTypeKind.Date);\n" +
            "        edmTypes.put(\"bigint\",EdmPrimitiveTypeKind.Int64);\n" +
            "        edmTypes.put(\"decimal\",EdmPrimitiveTypeKind.Decimal);\n" +
            "        edmTypes.put(\"smallint\",EdmPrimitiveTypeKind.Int16);\n" +
            "        edmTypes.put(\"bit\",EdmPrimitiveTypeKind.Binary);\n" +
            "        edmTypes.put(\"float\",EdmPrimitiveTypeKind.Double);\n" +
            "        edmTypes.put(\"int\",EdmPrimitiveTypeKind.Int32);\n" +
            "    }\n" +
            "\n" +
            "    public static FullQualifiedName getJavaByFlexberry(String type) {\n" +
            "        return edmTypes.get(type).getFullQualifiedName();\n" +
            "    }\n" +
            "}";

    public BuildHibernateToEdmTypesIFile() {
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return fileStructure;
    }

    @Override
    public String getFileDestination() {
        return Constants.HIBERNATEUTILS;
    }
}
