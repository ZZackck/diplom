package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 11.05.17.
 */
public class BuildHibernateQueryIFile implements IFilePattern {
    private String fileName = "HibernateQuery.java";
    private String mainPattern = "package model.HibernateUtils;\n" +
            "\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntityType;\n" +
            "import org.hibernate.Session;\n" +
            "import org.hibernate.query.Query;\n" +
            "import org.hibernate.resource.transaction.spi.TransactionStatus;\n" +
            "\n" +
            "import java.math.BigInteger;\n" +
            "import java.util.ArrayList;\n" +
            "import java.util.Collection;\n" +
            "import java.util.List;\n" +
            "\n" +
            "import org.apache.olingo.commons.api.data.Property;\n" +
            "import org.apache.olingo.commons.api.data.Entity;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 10.05.17.\n" +
            " */\n" +
            "public class HibernateQuery {\n" +
            "    private static Session session;\n" +
            "\n" +
            "    /**\n" +
            "     * Контсруктор и подклчение к сессии Гибернейт\n" +
            "     */\n" +
            "    public HibernateQuery() {\n" +
            "        session = HibernateFactory.getSession();\n" +
            "    }\n" +
            "\n" +
            "    public static List<?> getColumnDataTypes(String className) {\n" +
            "        if(!session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE))\n" +
            "                session.beginTransaction();\n" +
            "        List<?> newList = session.createSQLQuery(\"SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '\"+className+\"';\").list();\n" +
            "        if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {\n" +
            "            session.getTransaction().commit();\n" +
            "        }\n" +
            "        return newList;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Получение навзания колонок какой-либо таблицы\n" +
            "     * @param className\n" +
            "     * @return\n" +
            "     */\n" +
            "    public static List<?> getColumnNames(String className) {\n" +
            "        if(!session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE))\n" +
            "            session.beginTransaction();\n" +
            "        List<?> newList = session.createSQLQuery(\"SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '\"+className+\"';\").list();\n" +
            "        if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {\n" +
            "            session.getTransaction().commit();\n" +
            "        }\n" +
            "        return newList;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Получение всех записей из какой-либо таблицы\n" +
            "     * @param classNames\n" +
            "     * @return\n" +
            "     */\n" +
            "    public static List<Object[]> getFullData(String classNames) {\n" +
            "        String hql = \"SELECT * FROM \"+classNames.toString()+\"\";\n" +
            "        if(!session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE))\n" +
            "            session.beginTransaction();\n" +
            "        List<Object[]> listObject = new ArrayList<Object[]>((Collection<? extends Object[]>) session.createNativeQuery(hql).list());\n" +
            "        if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {\n" +
            "            session.getTransaction().commit();\n" +
            "        }\n" +
            "        return listObject;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Вставка записи в таблицу\n" +
            "     * @param tableName\n" +
            "     * @param propertiesList\n" +
            "     */\n" +
            "    public static void insertInto(String tableName, List<Property> propertiesList) {\n" +
            "        if(!session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE))\n" +
            "            session.beginTransaction();\n" +
            "        session.createNativeQuery(\"SET FOREIGN_KEY_CHECKS=0;\").executeUpdate();\n" +
            "        String insertString = new String(\"INSERT INTO \"+tableName+\" \"+insertString(propertiesList));\n" +
            "        Query query = session.createSQLQuery(insertString);\n" +
            "        query.executeUpdate();\n" +
            "        session.createNativeQuery(\"SET FOREIGN_KEY_CHECKS=1;\").executeUpdate();\n" +   
            "        if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {\n" +
            "            session.getTransaction().commit();\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Формирование параметров вставки\n" +
            "     * @param propertiesList\n" +
            "     * @return\n" +
            "     */\n" +
            "    private static String insertString(List<Property> propertiesList) {\n" +
            "        String params = new String(\"(%s) VALUES (%s)\");\n" +
            "        String columns = new String();\n" +
            "        String values = new String();\n" +
            "        for (Property prop : propertiesList) {\n" +
            "            columns += \"\"+prop.getName()+\", \";\n" +
            "            values += \"'\"+prop.getValue()+\"', \";\n" +
            "        }\n" +
            "\n" +
            "        if(columns.length() != 0) {\n" +
            "            columns = columns.substring(0,columns.length()-2);\n" +
            "            values = values.substring(0,values.length()-2);\n" +
            "        }\n" +
            "        params += \";\";\n" +
            "        return String.format(params,columns,values);\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Обновление записей в таблице\n" +
            "     * @param tableName\n" +
            "     * @param propertiesList\n" +
            "     * @param id\n" +
            "     */\n" +
            "    public static void update(String tableName, List<Property> propertiesList, Object id) {\n" +
            "        if(!session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE))\n" +
            "            session.beginTransaction();\n" +
            "        String insertString = new String(\"UPDATE \"+tableName+\" \"+updateString(propertiesList,id));\n" +
            "        session.createNativeQuery(\"SET FOREIGN_KEY_CHECKS=0;\").executeUpdate();\n" +
            "        Query query = session.createSQLQuery(insertString);\n" +
            "        query.executeUpdate();\n" +
            "        session.createNativeQuery(\"SET FOREIGN_KEY_CHECKS=1;\").executeUpdate();\n" +
            "        if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {\n" +
            "            session.getTransaction().commit();\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Формирование записи для обновления\n" +
            "     * @param propertiesList\n" +
            "     * @param id\n" +
            "     * @return\n" +
            "     */\n" +
            "    private static String updateString(List<Property> propertiesList, Object id) {\n" +
            "        String params = new String(\"SET %s\");\n" +
            "        String statement = new String();\n" +
            "        for (Property prop : propertiesList) {\n" +
            "            statement += prop.getName()+\"='\"+prop.getValue()+\"', \";\n" +
            "        }\n" +
            "\n" +
            "        if(statement.length() != 0) {\n" +
            "            statement = statement.substring(0,statement.length()-2);\n" +
            "            params += \" WHERE ID ='\"+(Integer)id+\"';\";\n" +
            "        }\n" +
            "        return String.format(params,statement);\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Удаление записи из таблицы\n" +
            "     * @param edmEntityType\n" +
            "     * @param entity\n" +
            "     */\n" +
            "    public static void delete(EdmEntityType edmEntityType, Entity entity) {\n" +
            "        if(!session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE))\n" +
            "            session.beginTransaction();\n" +
            "        String deleteString = new String(\"DELETE FROM \"+edmEntityType.getName()+\" WHERE ID='\"+(Integer)entity.getProperty(\"ID\").getValue()+\"';\");\n" +
            "        session.createNativeQuery(\"SET FOREIGN_KEY_CHECKS=0;\").executeUpdate();\n" +
            "        Query query = session.createSQLQuery(deleteString);\n" +
            "        query.executeUpdate();\n" +
            "        session.createNativeQuery(\"SET FOREIGN_KEY_CHECKS=1;\").executeUpdate();\n" +
            "        if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {\n" +
            "            session.getTransaction().commit();\n" +
            "        }\n" +
            "    }\n" +
            "}\n";


    public BuildHibernateQueryIFile() {
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return mainPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.HIBERNATEUTILS;
    }
}
