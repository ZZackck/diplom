package programm.CreateConfigFiles;

import com.sun.corba.se.impl.orbutil.closure.Constant;
import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 14.05.17.
 */
public class BuildPrimitiveProcessorFile implements IFilePattern {
    private String fileName = "DemoPrimitiveProcessor.java";

    public BuildPrimitiveProcessorFile() {
    }

    private String fileStructure = "package ODataUtils;\n" +
            "\n" +
            "import model.Utils.PrettyPrintClass;\n" +
            "import org.apache.olingo.commons.api.data.ContextURL;\n" +
            "import org.apache.olingo.commons.api.data.Entity;\n" +
            "import org.apache.olingo.commons.api.data.Property;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntitySet;\n" +
            "import org.apache.olingo.commons.api.edm.EdmPrimitiveType;\n" +
            "import org.apache.olingo.commons.api.edm.EdmProperty;\n" +
            "import org.apache.olingo.commons.api.format.ContentType;\n" +
            "import org.apache.olingo.commons.api.http.HttpHeader;\n" +
            "import org.apache.olingo.commons.api.http.HttpStatusCode;\n" +
            "import org.apache.olingo.server.api.*;\n" +
            "import org.apache.olingo.server.api.processor.PrimitiveProcessor;\n" +
            "import org.apache.olingo.server.api.serializer.ODataSerializer;\n" +
            "import org.apache.olingo.server.api.serializer.PrimitiveSerializerOptions;\n" +
            "import org.apache.olingo.server.api.serializer.SerializerResult;\n" +
            "import org.apache.olingo.server.api.uri.*;\n" +
            "import org.hibernate.Session;\n" +
            "\n" +
            "import java.io.InputStream;\n" +
            "import java.util.List;\n" +
            "import java.util.Locale;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 13.05.17.\n" +
            " */\n" +
            "public class DemoPrimitiveProcessor implements PrimitiveProcessor {\n" +
            "    private OData odata;\n" +
            "    private Storage storage;\n" +
            "    private ServiceMetadata serviceMetadata;\n" +
            "\n" +
            "    public DemoPrimitiveProcessor(Storage storage) {\n" +
            "        this.storage = storage;\n" +
            "    }\n" +
            "\n" +
            "    public void init(OData odata, ServiceMetadata serviceMetadata) {\n" +
            "        this.odata = odata;\n" +
            "        this.serviceMetadata = serviceMetadata;\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void readPrimitive(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo, ContentType contentType) throws ODataApplicationException, ODataLibraryException {\n" +
            "        List<UriResource> resourceParts = uriInfo.getUriResourceParts();\n" +
            "        // Note: only in our example we can rely that the first segment is the EntitySet\n" +
            "        UriResourceEntitySet uriEntityset = (UriResourceEntitySet) resourceParts.get(0);\n" +
            "        EdmEntitySet edmEntitySet = uriEntityset.getEntitySet();\n" +
            "        // the key for the entity\n" +
            "        List<UriParameter> keyPredicates = uriEntityset.getKeyPredicates();\n" +
            "\n" +
            "        // 1.2. retrieve the requested (Edm) property\n" +
            "        // the last segment is the Property\n" +
            "        UriResourceProperty uriProperty = (UriResourceProperty) resourceParts.get(resourceParts.size() -1);\n" +
            "        EdmProperty edmProperty = uriProperty.getProperty();\n" +
            "        String edmPropertyName = edmProperty.getName();\n" +
            "        // in our example, we know we have only primitive types in our model\n" +
            "        EdmPrimitiveType edmPropertyType = (EdmPrimitiveType) edmProperty.getType();\n" +
            "\n" +
            "        // 2. retrieve data from backend\n" +
            "        // 2.1. retrieve the entity data, for which the property has to be read\n" +
            "        Entity entity = storage.readEntityData(edmEntitySet, keyPredicates);\n" +
            "        if (entity == null) { // Bad request\n" +
            "            throw new ODataApplicationException(\"Entity not found\",\n" +
            "                    HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);\n" +
            "        }\n" +
            "\n" +
            "        // 2.2. retrieve the property data from the entity\n" +
            "        Property property = entity.getProperty(edmPropertyName);\n" +
            "        if (property == null) {\n" +
            "            throw new ODataApplicationException(\"Property not found\",\n" +
            "                    HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);\n" +
            "        }\n" +
            "\n" +
            "        // 3. serialize\n" +
            "        Object value = property.getValue();\n" +
            "        if (value != null) {\n" +
            "            // 3.1. configure the serializer\n" +
            "            ODataSerializer serializer = odata.createSerializer(contentType);\n" +
            "\n" +
            "            ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).navOrPropertyPath(edmPropertyName).build();\n" +
            "            PrimitiveSerializerOptions options = PrimitiveSerializerOptions.with().contextURL(contextUrl).build();\n" +
            "            // 3.2. serialize\n" +
            "            SerializerResult serializerResult = serializer.primitive(serviceMetadata, edmPropertyType, property, options);\n" +
            "            InputStream propertyStream = serializerResult.getContent();\n" +
            "\n" +
            "            if (contentType.getSubtype().toLowerCase().equals(\"json\"))\n" +
            "                propertyStream = PrettyPrintClass.prettyPrint(propertyStream);\n" +
            "\n" +
            "            //4. configure the response object\n" +
            "            oDataResponse.setContent(propertyStream);\n" +
            "            oDataResponse.setStatusCode(HttpStatusCode.OK.getStatusCode());\n" +
            "            oDataResponse.setHeader(HttpHeader.CONTENT_TYPE, contentType.toContentTypeString());\n" +
            "        }else{\n" +
            "            // in case there's no value for the property, we can skip the serialization\n" +
            "            oDataResponse.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void updatePrimitive(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo, ContentType contentType, ContentType contentType1) throws ODataApplicationException, ODataLibraryException {\n" +
            "        throw new ODataApplicationException(\"Not supported.\", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public void deletePrimitive(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo) throws ODataApplicationException, ODataLibraryException {\n" +
            "        throw new ODataApplicationException(\"Not supported.\", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);\n" +
            "    }\n" +
            "}\n";

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return fileStructure;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
