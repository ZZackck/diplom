package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 26.11.16.
 */
public class BuildHibernateFactoryIFile implements IFilePattern {
    private String fileName = "HibernateFactory.java";

    public BuildHibernateFactoryIFile() {
    }

    private String fileStructure = "package model.HibernateUtils;\n" +
            "\n" +
            "import org.hibernate.HibernateException;\n" +
            "import org.hibernate.Session;\n" +
            "import org.hibernate.SessionFactory;\n" +
            "import org.hibernate.cfg.Configuration;\n" +
            "\n" +
            "public class HibernateFactory {\n" +
            "\n" +
            "private static final SessionFactory sessionFactory;\n" +
            "\n" +
            "    /**\n" +
            "     * Создание гибернейт подклчения, конфигурация сессии\n" +
            "     */\n" +
            "    static {\n" +
            "        try {\n" +
            "            sessionFactory = new Configuration()\n" +
            "                                 .configure(\"META-INF/resources/hibernate.cfg.xml\")\n" +
            "                                 .buildSessionFactory();\n" +
            "            System.out.println(\"\");\n" +
            "            System.out.println(\"Creation complete, open session\");\n" +
            "        } catch (Throwable ex) {\n" +
            "            System.err.println(\"Initial SessionFactory creation failed.\" + ex);\n" +
            "            throw new ExceptionInInitializerError(ex);\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Получение сессии\n" +
            "     * @return\n" +
            "     * @throws HibernateException\n" +
            "     */\n" +
            "    public static Session getSession()\n" +
            "            throws HibernateException {\n" +
            "        return sessionFactory.openSession();\n" +
            "\t}\n" +
            "}";

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return fileStructure;
    }

    @Override
    public String getFileDestination() {
        return Constants.HIBERNATEUTILS;
    }
}
