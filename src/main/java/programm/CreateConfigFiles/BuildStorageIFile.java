package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 14.05.17.
 */
public class BuildStorageIFile implements IFilePattern{
    private String fileName = "Storage.java";

    private String mainPattern = "package ODataUtils;\n" +
            "\n" +
            "import model.HibernateUtils.HibernateQuery;\n" +
            "import model.Utils.ClassNames;\n" +
            "import org.apache.olingo.commons.api.data.Entity;\n" +
            "import org.apache.olingo.commons.api.data.EntityCollection;\n" +
            "import org.apache.olingo.commons.api.data.Property;\n" +
            "import org.apache.olingo.commons.api.data.ValueType;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntitySet;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntityType;\n" +
            "import org.apache.olingo.commons.api.ex.ODataRuntimeException;\n" +
            "import org.apache.olingo.commons.api.http.HttpMethod;\n" +
            "import org.apache.olingo.commons.api.http.HttpStatusCode;\n" +
            "import org.apache.olingo.server.api.ODataApplicationException;\n" +
            "import org.apache.olingo.server.api.uri.UriParameter;\n" +
            "import org.hibernate.Transaction;\n" +
            "\n" +
            "import java.math.BigInteger;\n" +
            "import java.net.URI;\n" +
            "import java.net.URISyntaxException;\n" +
            "import java.util.ArrayList;\n" +
            "import java.util.List;\n" +
            "import java.util.Locale;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 13.05.17.\n" +
            " */\n" +
            "public class Storage {\n" +
            "\n" +
            "    private List<Entity> entityList;\n" +
            "\n" +
            "    /**\n" +
            "     * Конструктор\n" +
            "     */\n" +
            "    public Storage() {\n" +
            "        entityList = new ArrayList<Entity>();\n" +
            "        initSampleData();\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Считывание колекции данных и ее обновление\n" +
            "     * @param edmEntitySet\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    public EntityCollection readEntitySetData(EdmEntitySet edmEntitySet) throws ODataApplicationException {\n" +
            "        refreshData();\n"+
            "\n" +
            "        EntityCollection retEntitySet = new EntityCollection();\n" +
            "\n" +
            "        for(Entity entity : this.entityList) {\n" +
            "            if(entity.getETag().equals(edmEntitySet.getEntityType().getName()))\n" +
            "            retEntitySet.getEntities().add(entity);\n" +
            "        }\n" +
            "\n" +
            "        return retEntitySet;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Получение сущности\n" +
            "     * @param edmEntitySet\n" +
            "     * @param keyParams\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    public Entity readEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams) throws ODataApplicationException{\n" +
            "        EdmEntityType edmEntityType = edmEntitySet.getEntityType();\n" +
            "        return getEntities(edmEntityType, keyParams);\n" +
            "    }\n" +
            "\n" +
            "\n" +
            "    private EntityCollection getEntities(EdmEntityType edmEntityType){\n" +
            "        EntityCollection retEntitySet = new EntityCollection();\n" +
            "\n" +
            "        for(Entity entity : this.entityList) {\n" +
            "            if (edmEntityType.getName().equals(entity.getETag()))\n" +
            "            retEntitySet.getEntities().add(entity);\n" +
            "        }\n" +
            "\n" +
            "        return retEntitySet;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Получение сущности по каким-либо ключам\n" +
            "     * @param edmEntityType\n" +
            "     * @param keyParams\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    private Entity getEntities(EdmEntityType edmEntityType, List<UriParameter> keyParams) throws ODataApplicationException{\n" +
            "        // the list of entities at runtime\n" +
            "        EntityCollection entitySet = getEntities(edmEntityType);\n" +
            "\n" +
            "        /*  generic approach  to find the requested entity */\n" +
            "        Entity requestedEntity = Util.findEntity(edmEntityType, entitySet, keyParams);\n" +
            "\n" +
            "        if(requestedEntity == null){\n" +
            "            /* this variable is null if our data doesn't contain an entity for the requested key\n" +
            "             Throw suitable exception*/\n" +
            "            throw new ODataApplicationException(\"Entity for requested key doesn't exist\",\n" +
            "                    HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);\n" +
            "        }\n" +
            "\n" +
            "        return requestedEntity;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Инициализация данных из БД\n" +
            "     */\n" +
            "    private void initSampleData() {\n" +
            "        List<Object[]> listObject = new ArrayList<>();\n" +
            "        List<?> columnList = new ArrayList<>();\n" +
            "        Entity entity = new Entity();\n" +
            "        Transaction transaction;\n" +
            "\n" +
            "        for(String classNames : ClassNames.getNames()) {\n" +
            "            listObject = HibernateQuery.getFullData(classNames.toString());\n" +
            "            columnList = HibernateQuery.getColumnNames(classNames.toString());\n" +
            "\n" +
            "            for (int i = 0; i < listObject.size(); i++) {\n" +
            "                entity = new Entity();\n" +
            "                for (int j = 0; j < columnList.size(); j++) {\n" +
            "                    entity.addProperty(new Property(null,columnList.get(j).toString(), ValueType.PRIMITIVE,(listObject.get(i))[j]))\n" +
            "                            .setETag(classNames.toString());\n" +
            "                }\n" +
            "                entity.setId(createId(classNames.toString(),listObject.get(i)[0]));\n" +
            "                entityList.add(entity);\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Создание ссылок на сущности\n" +
            "     * @param entitySetName\n" +
            "     * @param id\n" +
            "     * @return\n" +
            "     */\n" +
            "    private URI createId(String entitySetName, Object id) {\n" +
            "        try {\n" +
            "            return new URI(entitySetName + \"(\" + String.valueOf(id) + \")\");\n" +
            "        } catch (URISyntaxException e) {\n" +
            "            throw new ODataRuntimeException(\"Unable to create id for entity: \" + entitySetName, e);\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Создание сущности на метод POST\n" +
            "     * @param edmEntitySet\n" +
            "     * @param entityToCreate\n" +
            "     * @return\n" +
            "     */\n" +
            "    public Entity createEntityData(EdmEntitySet edmEntitySet, Entity entityToCreate) {\n" +
            "        EdmEntityType edmEntityType = edmEntitySet.getEntityType();\n" +
            "        refreshData();\n" +
            "        \n" +
            "        return createEntity(edmEntityType, entityToCreate);\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Создание сущности на метод POST\n" +
            "     * @param edmEntityType\n" +
            "     * @param entity\n" +
            "     * @return\n" +
            "     */\n" +
            "    private Entity createEntity(EdmEntityType edmEntityType, Entity entity) {\n" +
            "        Integer newId = 1;\n" +
            "        while (entityIdExist(newId,edmEntityType)) {\n" +
            "            newId ++;\n" +
            "        }\n" +
            "\n" +
            "        Property idProperty = entity.getProperty(\"ID\");\n" +
            "        if (idProperty != null) {\n" +
            "            idProperty.setValue(ValueType.PRIMITIVE, newId);\n" +
            "            } else {\n" +
            "                entity.getProperties().add(new Property(null, \"ID\", ValueType.PRIMITIVE, newId));\n" +
            "        }\n" +
            "        entity.setId(createId(edmEntityType.getName(), newId));\n" +
            "\n" +
            "        HibernateQuery.insertInto(edmEntityType.getName(), entity.getProperties());\n" +
            "        return entity;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Проверка ключа на существоввание\n" +
            "     * @param id\n" +
            "     * @param edmEntityType\n" +
            "     * @return\n" +
            "     */\n" +
            "    private boolean entityIdExist(int id, EdmEntityType edmEntityType) {\n" +
            "        for (Entity entity : this.entityList) {\n" +
            "            if (entity.getETag().equals(edmEntityType.getName())) {\n" +
            "                Integer existingID = (Integer) entity.getProperty(\"ID\").getValue();\n" +
            "                if (existingID == id) {\n" +
            "                    return true;\n" +
            "                }\n" +
            "            }\n" +
            "        }\n" +
            "        return false;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Метод UPDATE\n" +
            "     * @param edmEntityType\n" +
            "     * @param keyParams\n" +
            "     * @param updateEntity\n" +
            "     * @param httpMethod\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    public void updateEntityData(EdmEntityType edmEntityType, List<UriParameter> keyParams, Entity updateEntity,\n" +
            "        HttpMethod httpMethod) throws ODataApplicationException {\n" +
            "        refreshData();\n" +
            "\n" +
            "        Entity entity = getEntities(edmEntityType, keyParams);\n" +
            "        if (entity == null) {\n" +
            "            throw new ODataApplicationException(\"Entity not found\", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);\n" +
            "        }\n" +
            "\n" +
            "        HibernateQuery.update(edmEntityType.getName(), updateEntity.getProperties(), entity.getProperty(\"ID\").getValue());\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Метод DELETE\n" +
            "     * @param edmEntitySet\n" +
            "     * @param keyParams\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    public void deleteEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)\n" +
            "        throws ODataApplicationException {\n" +
            "        refreshData();\n"+
            "\n" +
            "        EdmEntityType edmEntityType = edmEntitySet.getEntityType();\n" +
            "        deleteEntity(edmEntityType, keyParams);\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Метод DELETE\n" +
            "     * @param edmEntityType\n" +
            "     * @param keyParams\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    private void deleteEntity(EdmEntityType edmEntityType, List<UriParameter> keyParams)\n" +
            "        throws ODataApplicationException {\n" +
            "\n" +
            "        Entity entity = getEntities(edmEntityType, keyParams);\n" +
            "\n" +
            "        if (entity == null) {\n" +
            "            throw new ODataApplicationException(\"Entity not found\", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);\n" +
            "        }\n" +
            "\n" +
            "        HibernateQuery.delete(edmEntityType, entity);\n" +
            "    }\n" +
            "\n" +
            "    private void refreshData() {\n" +
            "        entityList.clear();\n" +
            "        initSampleData();\n" +
            "    }\n" +
            "}";

    public BuildStorageIFile() {
    }

    @Override

    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return mainPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
