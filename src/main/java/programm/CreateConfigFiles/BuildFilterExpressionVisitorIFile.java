package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 18.05.17.
 */
public class BuildFilterExpressionVisitorIFile  implements IFilePattern{
    private String fileName = "FilterExpressionVisitor.java";

    private String mainPattern = "package ODataUtils;\n" +
            "\n" +
            "import org.apache.olingo.commons.api.data.Entity;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEnumType;\n" +
            "import org.apache.olingo.commons.api.edm.EdmType;\n" +
            "import org.apache.olingo.commons.api.http.HttpStatusCode;\n" +
            "import org.apache.olingo.commons.core.edm.primitivetype.EdmString;\n" +
            "import org.apache.olingo.server.api.ODataApplicationException;\n" +
            "import org.apache.olingo.server.api.uri.UriResource;\n" +
            "import org.apache.olingo.server.api.uri.UriResourcePrimitiveProperty;\n" +
            "import org.apache.olingo.server.api.uri.queryoption.expression.*;\n" +
            "\n" +
            "import java.math.BigInteger;\n" +
            "import java.util.List;\n" +
            "import java.util.Locale;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 18.05.17.\n" +
            " */\n" +
            "public class FilterExpressionVisitor implements ExpressionVisitor<Object> {\n" +
            "   private Entity currentEntity;\n" +
            "\n" +
            "    public FilterExpressionVisitor(Entity currentEntity) throws ODataApplicationException {\n" +
            "        this.currentEntity = currentEntity;\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitMember(final Member member) throws ExpressionVisitException, ODataApplicationException {\n" +
            "    // To keeps things simple, this tutorial allows only primitive properties.\n" +
            "    // We have faith that the java type of Edm.Int32 is Integer\n" +
            "    final List<UriResource> uriResourceParts = member.getResourcePath().getUriResourceParts();\n" +
            "\n" +
            "    /* Make sure that the resource path of the property contains only a single segment and a primitive property\n" +
            "     has been addressed. We can be sure, that the property exists because the UriParser checks if the\n" +
            "     property has been defined in service metadata document.*/\n" +
            "\n" +
            "     if(uriResourceParts.size() == 1 && uriResourceParts.get(0) instanceof UriResourcePrimitiveProperty) {\n" +
            "        UriResourcePrimitiveProperty uriResourceProperty = (UriResourcePrimitiveProperty) uriResourceParts.get(0);\n" +
            "        return currentEntity.getProperty(uriResourceProperty.getProperty().getName()).getValue();\n" +
            "        } else {\n" +
            "         /* The OData specification allows in addition complex properties and navigation properties\n" +
            "          with a target cardinality 0..1 or 1.\n" +
            "          This means any combination can occur e.g. Supplier/Address/City\n" +
            "           -> Navigation properties  Supplier\n" +
            "          -> Complex Property       Address\n" +
            "          -> Primitive Property     City\n" +
            "         For such cases the resource path returns a list of UriResourceParts*/\n" +
            "        throw new ODataApplicationException(\"Only primitive properties are implemented in filter expressions\",\n" +
            "                  HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "         }\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitLiteral(Literal literal) throws ExpressionVisitException, ODataApplicationException {\n" +
            "    // To keep this tutorial simple, our filter expression visitor supports only Edm.Int32 and Edm.String\n" +
            "    // In real world scenarios it can be difficult to guess the type of an literal.\n" +
            "     // We can be sure, that the literal is a valid OData literal because the URI Parser checks\n" +
            "     // the lexicographical structure\n" +
            "\n" +
            "     // String literals start and end with an single quotation mark\n" +
            "     String literalAsString = literal.getText();\n" +
            "      if(literal.getType() instanceof EdmString) {\n" +
            "            String stringLiteral = \"\";\n" +
            "            if(literal.getText().length() > 2) {\n" +
            "                stringLiteral = literalAsString.substring(1, literalAsString.length() - 1);\n" +
            "            }\n" +
            "            return stringLiteral;\n" +
            "            } else {\n" +
            "            // Try to convert the literal into an Java Integer\n" +
            "            try {\n" +
            "                return Integer.parseInt(literalAsString);\n" +
            "                } catch(NumberFormatException e) {\n" +
            "                throw new ODataApplicationException(\"Only Edm.Int32 and Edm.String literals are implemented\",\n" +
            "                        HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "                }\n" +
            "            }\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitUnaryOperator(UnaryOperatorKind operator, Object operand)\n" +
            "        throws ExpressionVisitException, ODataApplicationException {\n" +
            "        // OData allows two different unary operators. We have to take care, that the type of the operand fits to operand\n" +
            "        if(operator == UnaryOperatorKind.NOT && operand instanceof Boolean) {\n" +
            "            // 1.) boolean negation\n" +
            "            return !(Boolean) operand;\n" +
            "                } else if(operator == UnaryOperatorKind.MINUS && operand instanceof Integer){\n" +
            "            // 2.) arithmetic minus\n" +
            "            return -(Integer) operand;\n" +
            "            }\n" +
            "        // Operation not processed, throw an exception\n" +
            "        throw new ODataApplicationException(\"Invalid type for unary operator\",\n" +
            "                HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitBinaryOperator(BinaryOperatorKind operator, Object left, Object right)\n" +
            "            throws ExpressionVisitException, ODataApplicationException {\n" +
            "         /*Binary Operators are split up in three different kinds. Up to the kind of the operator it can be applied\n" +
            "         to different types\n" +
            "           - Arithmetic operations like add, minus, modulo, etc. are allowed on numeric types like Edm.Int32\n" +
            "           - Logical operations are allowed on numeric types and also Edm.String\n" +
            "           - Boolean operations like and, or are allowed on Edm.Boolean*/\n" +
            "\n" +
            "        if (operator == BinaryOperatorKind.ADD\n" +
            "        || operator == BinaryOperatorKind.MOD\n" +
            "        || operator == BinaryOperatorKind.MUL\n" +
            "        || operator == BinaryOperatorKind.DIV\n" +
            "        || operator == BinaryOperatorKind.SUB) {\n" +
            "            return evaluateArithmeticOperation(operator, left, right);\n" +
            "            } else if (operator == BinaryOperatorKind.EQ\n" +
            "        || operator == BinaryOperatorKind.NE\n" +
            "        || operator == BinaryOperatorKind.GE\n" +
            "        || operator == BinaryOperatorKind.GT\n" +
            "        || operator == BinaryOperatorKind.LE\n" +
            "        || operator == BinaryOperatorKind.LT) {\n" +
            "            return evaluateComparisonOperation(operator, left, right);\n" +
            "            } else if (operator == BinaryOperatorKind.AND\n" +
            "        || operator == BinaryOperatorKind.OR) {\n" +
            "            return evaluateBooleanOperation(operator, left, right);\n" +
            "            } else {\n" +
            "            throw new ODataApplicationException(\"Binary operation \" + operator.name() + \" is not implemented\",\n" +
            "                    HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "            }\n" +
            "        }\n" +
            "\n" +
            "    private Object evaluateBooleanOperation(BinaryOperatorKind operator, Object left, Object right)\n" +
            "            throws ODataApplicationException {\n" +
            "        // First check that both operands are of type Boolean\n" +
            "        if(left instanceof Boolean && right instanceof Boolean) {\n" +
            "            Boolean valueLeft = (Boolean) left;\n" +
            "            Boolean valueRight = (Boolean) right;\n" +
            "            // Than calculate the result value\n" +
            "            if(operator == BinaryOperatorKind.AND) {\n" +
            "                return valueLeft && valueRight;\n" +
            "                } else {\n" +
            "                // OR\n" +
            "                return valueLeft || valueRight;\n" +
            "                }\n" +
            "            } else {\n" +
            "            throw new ODataApplicationException(\"Boolean operations needs two numeric operands\",\n" +
            "                    HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);\n" +
            "            }\n" +
            "    }\n" +
            "\n" +
            "    private Object evaluateComparisonOperation(BinaryOperatorKind operator, Object left, Object right)\n" +
            "            throws ODataApplicationException {\n" +
            "        // All types in our tutorial supports all logical operations, but we have to make sure that the types are equals\n" +
            "        if(left instanceof BigInteger) {\n" +
            "            Integer i = ((BigInteger)left).intValue();\n" +
            "            left = i;\n" +
            "        }\n" +
            "\n" +
            "        if(left.getClass().equals(right.getClass()) && left instanceof Comparable) {\n" +
            "            // Luckily all used types String, Boolean and also Integer support the interface Comparable\n" +
            "            int result;\n" +
            "            if(left instanceof Integer) {\n" +
            "                result = ((Comparable<Integer>) (Integer)left).compareTo((Integer) right);\n" +
            "                } else if(left instanceof String) {\n" +
            "                result = ((Comparable<String>) (String)left).compareTo((String) right);\n" +
            "                } else if(left instanceof Boolean) {\n" +
            "                result = ((Comparable<Boolean>) (Boolean)left).compareTo((Boolean) right);\n" +
            "                } else {\n" +
            "                throw new ODataApplicationException(\"Class \" + left.getClass().getCanonicalName() + \" not expected\",\n" +
            "                            HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ENGLISH);\n" +
            "                }\n" +
            "\n" +
            "            if (operator == BinaryOperatorKind.EQ) {\n" +
            "                return result == 0;\n" +
            "                } else if (operator == BinaryOperatorKind.NE) {\n" +
            "                return result != 0;\n" +
            "                } else if (operator == BinaryOperatorKind.GE) {\n" +
            "                return result >= 0;\n" +
            "                } else if (operator == BinaryOperatorKind.GT) {\n" +
            "                return result > 0;\n" +
            "                } else if (operator == BinaryOperatorKind.LE) {\n" +
            "                return result <= 0;\n" +
            "                } else {\n" +
            "                 // BinaryOperatorKind.LT\n" +
            "                return result < 0;\n" +
            "                }\n" +
            "            } else {\n" +
            "            throw new ODataApplicationException(\"Comparision needs two equal types\",\n" +
            "                    HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);\n" +
            "            }\n" +
            "    }\n" +
            "\n" +
            "    private Object evaluateArithmeticOperation(BinaryOperatorKind operator, Object left,\n" +
            "                                               Object right) throws ODataApplicationException {\n" +
            "    // First check if the type of both operands is numerical\n" +
            "        if(left instanceof Integer && right instanceof Integer) {\n" +
            "            Integer valueLeft = (Integer) left;\n" +
            "            Integer valueRight = (Integer) right;\n" +
            "            // Than calculate the result value\n" +
            "            if(operator == BinaryOperatorKind.ADD) {\n" +
            "                    return valueLeft + valueRight;\n" +
            "                } else if(operator == BinaryOperatorKind.SUB) {\n" +
            "                    return valueLeft - valueRight;\n" +
            "                } else if(operator == BinaryOperatorKind.MUL) {\n" +
            "                    return valueLeft * valueRight;\n" +
            "                } else if(operator == BinaryOperatorKind.DIV) {\n" +
            "                    return valueLeft / valueRight;\n" +
            "                } else {\n" +
            "                // BinaryOperatorKind,MOD\n" +
            "                    return valueLeft % valueRight;\n" +
            "                }\n" +
            "            } else {\n" +
            "            throw new ODataApplicationException(\"Arithmetic operations needs two numeric operands\",\n" +
            "                    HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);\n" +
            "            }\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitMethodCall(MethodKind methodCall, List<Object> parameters)\n" +
            "            throws ExpressionVisitException, ODataApplicationException {\n" +
            "        if(methodCall == MethodKind.CONTAINS) {\n" +
            "             /*\"Contains\" gets two parameters, both have to be of type String\n" +
            "             e.g. /Products?$filter=contains(Description, '1024 MB')\n" +
            "             First the method visistMember is called, which returns the current String value of the property.\n" +
            "             After that the method visitLiteral is called with the string literal '1024 MB',\n" +
            "             which returns a String\n" +
            "             Both String values are passed to visitMethodCall.*/\n" +
            "            if(parameters.get(0) instanceof String && parameters.get(1) instanceof String) {\n" +
            "                String valueParam1 = (String) parameters.get(0);\n" +
            "                String valueParam2 = (String) parameters.get(1);\n" +
            "                    return valueParam1.contains(valueParam2);\n" +
            "                } else {\n" +
            "                throw new ODataApplicationException(\"Contains needs two parametes of type Edm.String\",\n" +
            "                        HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);\n" +
            "                }\n" +
            "            } else {\n" +
            "            throw new ODataApplicationException(\"Method call \" + methodCall + \" not implemented\",\n" +
            "                    HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "            }\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitTypeLiteral(EdmType type) throws ExpressionVisitException, ODataApplicationException {\n" +
            "        throw new ODataApplicationException(\"Type literals are not implemented\",\n" +
            "                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitAlias(String aliasName) throws ExpressionVisitException, ODataApplicationException {\n" +
            "        throw new ODataApplicationException(\"Aliases are not implemented\",\n" +
            "                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitEnum(EdmEnumType type, List<String> enumValues)\n" +
            "            throws ExpressionVisitException, ODataApplicationException {\n" +
            "        throw new ODataApplicationException(\"Enums are not implemented\",\n" +
            "                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitLambdaExpression(String lambdaFunction, String lambdaVariable, Expression expression)\n" +
            "            throws ExpressionVisitException, ODataApplicationException {\n" +
            "        throw new ODataApplicationException(\"Lamdba expressions are not implemented\",\n" +
            "                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "    }\n" +
            "\n" +
            "    @Override\n" +
            "    public Object visitLambdaReference(String variableName)\n" +
            "            throws ExpressionVisitException, ODataApplicationException {\n" +
            "        throw new ODataApplicationException(\"Lamdba references are not implemented\",\n" +
            "                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "    }\n" +
            "}\n";

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return mainPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
