package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 22.12.16.
 */
public class BuildWebXMLIFile implements IFilePattern {
    private String webName = "web.xml";
    private String webPattern = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<web-app xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://java.sun.com/xml/ns/javaee\"\n" +
            "         xsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd\" id=\"WebApp_ID\" version=\"3.0\">\n" +
            "\n" +
            "<display-name>modelHibernate</display-name>\n" +
            "<servlet>\n" +
            "    <servlet-name>ODataServlet</servlet-name>\n" +
            "    <servlet-class>servlet.ODataServlet</servlet-class>\n" +
            "    <load-on-startup>1</load-on-startup>\n" +
            "</servlet>\n" +
            "\n" +
            "<servlet-mapping>\n" +
            "    <servlet-name>ODataServlet</servlet-name>\n" +
            "    <url-pattern>/ODataServlet.svc/*</url-pattern>\n" +
            "</servlet-mapping>\n" +
            "\n" +
            "</web-app>";

    public BuildWebXMLIFile() {
    }

    @Override
    public String getFileName() {
        return webName;
    }

    @Override
    public String getFileStructure() {
        return webPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.WEBAPP;
    }
}
