package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 26.11.16.
 */
public class BuildPomIFile implements IFilePattern {
    private String fileName = "pom.xml";

    public BuildPomIFile() {

    }

    private String filePattern = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<project xmlns=\"http://maven.apache.org/POM/4.0.0\"\n" +
            "         xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
            "         xsi:schemaLocation=\"http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd\">\n" +
            "    <modelVersion>4.0.0</modelVersion>\n" +
            "\n" +
            "    <groupId>programm</groupId>\n" +
            "    <artifactId>GeneratedApplication</artifactId>\n" +
            "    <version>1.0</version>\n" +
            "    <packaging>war</packaging>\n" +
            "\n" +
            "\n" +
            "    <properties>\n" +
            "        <javax.version>2.5</javax.version>\n" +
            "        <odata.version>4.0.0</odata.version>\n" +
            "        <slf4j.version>1.7.7</slf4j.version>\n" +
            "    </properties>\n" +
            "\n" +
            "    <build>\n" +
            "       <resources>\n" +
            "           <resource>\n" +
            "        \t<targetPath>META-INF/resources</targetPath>\n" +
            "        \t<filtering>false</filtering>\n" +
            "        \t<directory>${basedir}/src/main/resources</directory>\n" +
            "        \t<includes>\n" +
            "          \t\t<include>**/*.xml</include>\n" +
            "        \t</includes>\n" +
            " \t  </resource>\n" +
            "   \t</resources>\n" +
            "        <plugins>\n" +
            "            <plugin>\n" +
            "                <groupId>org.apache.maven.plugins</groupId>\n" +
            "                <artifactId>maven-compiler-plugin</artifactId>\n" +
            "                <version>3.6.0</version>\n" +
            "                <configuration>\n" +
            "                    <source>1.8</source>\n" +
            "                    <target>1.8</target>\n" +
            "                </configuration>\n" +
            "            </plugin>\n" +
            "               <plugin>\n" +
            "                <groupId>org.apache.maven.plugins</groupId>\n" +
            "                <artifactId>maven-war-plugin</artifactId>\n" +
            "                <version>3.0.0</version>\n" +
            "                <configuration>\n" +
            "                    <failOnMissingWebXml>false</failOnMissingWebXml>\n" +
            "                </configuration>\n" +
            "               </plugin>\n" +
            "        </plugins>\n" +
            "    </build>\n" +
            "\n" +
            "\n" +
            "   <dependencies>\n" +
            "      <dependency>\n" +
            "            <groupId>org.hibernate</groupId>\n" +
            "            <artifactId>hibernate-core</artifactId>\n" +
            "            <version>5.2.5.Final</version>\n" +
            "        </dependency>\n" +
            "      <dependency>\n" +
            "           <groupId>mysql</groupId>\n" +
            "           <artifactId>mysql-connector-java</artifactId>\n" +
            "           <version>6.0.5</version>\n" +
            "      </dependency>\n" +
            "       <dependency>\n" +
            "            <groupId>javax.servlet</groupId>\n" +
            "            <artifactId>javax.servlet-api</artifactId>\n" +
            "            <version>4.0.0-b01</version>\n" +
            "           <scope>provided</scope>\n" +
            "        </dependency>\n" +
            "        <dependency>\n" +
            "            <groupId>javax</groupId>\n" +
            "            <artifactId>javaee-api</artifactId>\n" +
            "            <version>7.0</version>\n" +
            "        </dependency>\n" +
            "        <dependency>\n" +
            "            <groupId>javax</groupId>\n" +
            "            <artifactId>javaee-web-api</artifactId>\n" +
            "            <version>7.0</version>\n" +
            "            <scope>provided</scope>\n" +
            "        </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.apache.olingo</groupId>\n" +
            "           <artifactId>odata-server-api</artifactId>\n" +
            "           <version>4.3.0</version>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.apache.olingo</groupId>\n" +
            "           <artifactId>odata-server-core</artifactId>\n" +
            "           <version>4.3.0</version>\n" +
            "           <scope>runtime</scope>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.apache.olingo</groupId>\n" +
            "           <artifactId>odata-commons-api</artifactId>\n" +
            "           <version>4.3.0</version>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.apache.olingo</groupId>\n" +
            "           <artifactId>odata-commons-core</artifactId>\n" +
            "           <version>4.3.0</version>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.slf4j</groupId>\n" +
            "           <artifactId>slf4j-simple</artifactId>\n" +
            "           <version>1.7.25</version>\n" +
            "           <scope>runtime</scope>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.slf4j</groupId>\n" +
            "           <artifactId>slf4j-api</artifactId>\n" +
            "           <version>1.7.25</version>\n" +
            "           <scope>compile</scope>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>com.google.code.gson</groupId>\n" +
            "           <artifactId>gson</artifactId>\n" +
            "           <version>2.8.0</version>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>commons-io</groupId>\n" +
            "           <artifactId>commons-io</artifactId>\n" +
            "           <version>2.5</version>\n" +
            "       </dependency>\n" +
            "       <dependency>\n" +
            "           <groupId>org.reflections</groupId>\n" +
            "           <artifactId>reflections</artifactId>\n" +
            "           <version>0.9.11</version>\n" +
            "       </dependency>\n" +
            "   </dependencies>\n" +
            "</project>";

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return filePattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.ROOT;
    }

}
