package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 22.12.16.
 */
public class BuildServletIFile implements IFilePattern {
    private String servletName = "ODataServlet.java";

    public BuildServletIFile() {
    }

    private String servletPattern = "package servlet;\n" +
            "\n" +
            "import ODataUtils.*;\n" +
            "import model.HibernateUtils.HibernateQuery;\n" +
            "import model.Utils.ClassNames;\n" +
            "import org.apache.olingo.commons.api.edmx.EdmxReference;\n" +
            "import org.apache.olingo.server.api.OData;\n" +
            "import org.apache.olingo.server.api.ODataHttpHandler;\n" +
            "import org.apache.olingo.server.api.ServiceMetadata;\n" +
            "\n" +
            "import javax.servlet.ServletConfig;\n" +
            "import javax.servlet.ServletException;\n" +
            "import javax.servlet.http.HttpServlet;\n" +
            "import javax.servlet.http.HttpServletRequest;\n" +
            "import javax.servlet.http.HttpServletResponse;\n" +
            "import javax.servlet.http.HttpSession;\n" +
            "import java.io.IOException;\n" +
            "import java.util.ArrayList;\n" +
            "\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 22.12.16.\n" +
            " */\n" +
            "public class ODataServlet extends HttpServlet{\n" +
            "    private HibernateQuery hibernateQuery;\n" +
            "\n" +
            "    /**\n" +
            "     * Конфигурация сервлета, его инициализация\n" +
            "     * @param servletConfig\n" +
            "     * @throws ServletException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void init(ServletConfig servletConfig) throws ServletException {\n" +
            "        super.init(servletConfig);\n" +
            "        ClassNames classNames = new ClassNames();\n" +
            "        hibernateQuery = new HibernateQuery();\n" +
            "\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Метод для HTTP-запросов\n" +
            "     * @param req\n" +
            "     * @param resp\n" +
            "     * @throws ServletException\n" +
            "     * @throws IOException\n" +
            "     */\n" +
            "    protected void service(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {\n" +
            "        try {\n" +
            "            HttpSession sess = req.getSession(true);\n" +
            "            Storage storage = (Storage) sess.getAttribute(Storage.class.getName());\n" +
            "            if (storage == null) {\n" +
            "                storage = new Storage();\n" +
            "                sess.setAttribute(Storage.class.getName(), storage);\n" +
            "            }\n" +
            "            // create odata handler and configure it with CsdlEdmProvider and Processor\n" +
            "            OData odata = OData.newInstance();\n" +
            "            ServiceMetadata edm = odata.createServiceMetadata(new DemoEdmProvider(), new ArrayList<EdmxReference>());\n" +
            "            ODataHttpHandler handler = odata.createHandler(edm);\n" +
            "            /**\n" +
            "             * Регистрация классов для работы с сущностями OData\n" +
            "             */\n" +
            "            handler.register(new DemoEntityCollectionProcessor(storage));\n" +
            "            handler.register(new DemoEntityProcessor(storage));\n" +
            "            handler.register(new DemoPrimitiveProcessor(storage));\n" +
            "            // let the handler do the work\n" +
            "            handler.process(req, resp);\n" +
            "\n" +
            "        } catch (RuntimeException e) {\n" +
            "            throw new ServletException(e);\n" +
            "        }\n" +
            "    }\n" +
            "}";


    @Override
    public String getFileName() {
        return servletName;
    }

    @Override
    public String getFileStructure() {
        return servletPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.SERVLET;
    }
}
