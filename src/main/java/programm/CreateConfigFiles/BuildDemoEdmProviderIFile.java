package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 11.05.17.
 */
public class BuildDemoEdmProviderIFile implements IFilePattern {
    private String fileName = "DemoEdmProvider.java";

    public BuildDemoEdmProviderIFile() {
    }

    private String mainPattern = "package ODataUtils;\n" +
            "\n" +
            "import model.HibernateUtils.HibernateQuery;\n" +
            "import model.HibernateUtils.HibernateToEdmTypesMap;\n" +
            "import model.Utils.ClassNames;\n" +
            "import org.apache.olingo.commons.api.edm.FullQualifiedName;\n" +
            "import org.apache.olingo.commons.api.edm.provider.*;\n" +
            "import org.apache.olingo.commons.api.ex.ODataException;\n" +
            "\n" +
            "import java.util.ArrayList;\n" +
            "import java.util.Collections;\n" +
            "import java.util.List;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 06.05.17.\n" +
            " */\n" +
            "public class DemoEdmProvider extends CsdlAbstractEdmProvider {\n" +
            "    public static String NAMESPACE = \"\";\n" +
            "    public static final String CONTAINER_NAME = \"Container\";\n" +
            "    public static final FullQualifiedName CONTAINER = new FullQualifiedName(NAMESPACE, CONTAINER_NAME);\n" +
            "\n" +
            "    /**\n" +
            "     * Конструктор\n" +
            "     */\n" +
            "    public DemoEdmProvider() {\n" +
            "        NAMESPACE = ClassNames.containerName();\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Получение типов данных для каждой колонки у каждой таблицы и присвоение ключевого поля (Объявление типов данных для кажой сущности)\n" +
            "     * @param fullQualifiedName\n" +
            "     * @return\n" +
            "     * @throws ODataException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public CsdlEntityType getEntityType(FullQualifiedName fullQualifiedName) throws ODataException {\n" +
            "\n" +
            "        String className = fullQualifiedName.getName();\n" +
            "\n" +
            "        List<?> columnList = HibernateQuery.getColumnNames(className);\n" +
            "        List<?> columnDataType = HibernateQuery.getColumnDataTypes(className);\n" +
            "\n" +
            "        List<CsdlProperty> propertyList = new ArrayList<>();\n" +
            "        for (int i = 0; i < columnDataType.size(); i++) {\n" +
            "            propertyList.add(new CsdlProperty().setName(columnList.get(i).toString()).setType(HibernateToEdmTypesMap.getJavaByFlexberry(columnDataType.get(i).toString().toLowerCase())));\n" +
            "        }\n" +
            "\n" +
            "        CsdlPropertyRef propertyRef = new CsdlPropertyRef();\n" +
            "        propertyRef.setName(\"ID\");\n" +
            "\n" +
            "        CsdlEntityType entityType = new CsdlEntityType();\n" +
            "\n" +
            "        entityType.setName(className);\n" +
            "        entityType.setProperties(propertyList);\n" +
            "        entityType.setKey(Collections.singletonList(propertyRef));\n" +
            "\n" +
            "        return entityType;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Утверждение вызова списка записей для какой либо сущности\n" +
            "     * @param fullQualifiedName\n" +
            "     * @param s\n" +
            "     * @return\n" +
            "     * @throws ODataException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public CsdlEntitySet getEntitySet(FullQualifiedName fullQualifiedName, String s) throws ODataException {\n" +
            "\n" +
            "        if(fullQualifiedName.equals(CONTAINER)){\n" +
            "            FullQualifiedName CLASS_MODEL_NAME;\n" +
            "            for(String classNames : ClassNames.getNames()) {\n" +
            "                String newString = classNames.toString();\n" +
            "                if (newString.equals(s)) {\n" +
            "                    CsdlEntitySet entitySet = new CsdlEntitySet();\n" +
            "                    entitySet.setName(classNames.toString());\n" +
            "                    CLASS_MODEL_NAME = new FullQualifiedName(NAMESPACE,classNames.toString());\n" +
            "                    entitySet.setType(CLASS_MODEL_NAME);\n" +
            "                    return entitySet;\n" +
            "                }\n" +
            "            }\n" +
            "        }\n" +
            "\n" +
            "        return null;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Информация об обслуживащем контейнере, которая будет выведена на начальном окне ODataServlet\n" +
            "     * @param fullQualifiedName\n" +
            "     * @return\n" +
            "     * @throws ODataException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public CsdlEntityContainerInfo getEntityContainerInfo(FullQualifiedName fullQualifiedName) throws ODataException {\n" +
            "        if (fullQualifiedName == null || fullQualifiedName.equals(CONTAINER)) {\n" +
            "            CsdlEntityContainerInfo entityContainerInfo = new CsdlEntityContainerInfo();\n" +
            "            entityContainerInfo.setContainerName(CONTAINER);\n" +
            "            return entityContainerInfo;\n" +
            "        }\n" +
            "\n" +
            "        return null;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Корневой элемент переноса сущностей\n" +
            "     * @return\n" +
            "     * @throws ODataException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public List<CsdlSchema> getSchemas() throws ODataException {\n" +
            "\n" +
            "        CsdlSchema schema = new CsdlSchema();\n" +
            "        schema.setNamespace(NAMESPACE);\n" +
            "\n" +
            "        List<CsdlEntityType> entityTypes = new ArrayList<CsdlEntityType>();\n" +
            "\n" +
            "        FullQualifiedName CLASS_MODEL_NAME;\n" +
            "\n" +
            "        for(String classNames : ClassNames.getNames()) {\n" +
            "            CLASS_MODEL_NAME = new FullQualifiedName(NAMESPACE,classNames.toString());\n" +
            "            entityTypes.add(getEntityType(CLASS_MODEL_NAME));\n" +
            "        }\n" +
            "\n" +
            "        schema.setEntityTypes(entityTypes);\n" +
            "\n" +
            "        schema.setEntityContainer(getEntityContainer());\n" +
            "        List<CsdlSchema> schemas = new ArrayList<CsdlSchema>();\n" +
            "        schemas.add(schema);\n" +
            "\n" +
            "        return schemas;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Контейнер получает данные (каркас), которые необходимы для EntitySet\n" +
            "     * @return\n" +
            "     * @throws ODataException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public CsdlEntityContainer getEntityContainer() throws ODataException {\n" +
            "        List<CsdlEntitySet> entitySets = new ArrayList<CsdlEntitySet>();\n" +
            "\n" +
            "        for(String classNames : ClassNames.getNames()) {\n" +
            "            entitySets.add(getEntitySet(CONTAINER, classNames.toString()));\n" +
            "        }\n" +
            "\n" +
            "        CsdlEntityContainer entityContainer = new CsdlEntityContainer();\n" +
            "        entityContainer.setName(CONTAINER_NAME);\n" +
            "        entityContainer.setEntitySets(entitySets);\n" +
            "\n" +
            "        return entityContainer;\n" +
            "    }\n" +
            "\n" +
            "}";

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return mainPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
