package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 11.12.16.
 */
public class BuildShellIFile implements IFilePattern {
    private String fileName = "build.sh";
    private String buildPattern = "mvn package\n"+
                    "cd ./target\n"+
                    "var=$(pwd)\n"+
            "cp GeneratedApplication-*.war GeneratedApplication.war\n"+
            "wget --http-user=tomcat --http-password=tomcat \"http://localhost:8080/manager/text/deploy?war=file:$var/GeneratedApplication.war&path=/GeneratedApplication\" -O - \n"+
            "echo Необходимо предоставить права суперпользователя\n"+
            "cd /opt/tomcat/apache-tomcat/webapps\n"+
            "sudo chmod -R 777 *";

    public BuildShellIFile() {
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return buildPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.ROOT;
    }
}
