package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 14.05.17.
 */
public class BuildUtilIFile implements IFilePattern{
    private String fileName = "Util.java";

    private String fileStructure = "package ODataUtils;\n" +
            "\n" +
            "import org.apache.olingo.commons.api.data.Entity;\n" +
            "import org.apache.olingo.commons.api.data.EntityCollection;\n" +
            "import org.apache.olingo.commons.api.edm.*;\n" +
            "import org.apache.olingo.commons.api.http.HttpStatusCode;\n" +
            "import org.apache.olingo.server.api.ODataApplicationException;\n" +
            "import org.apache.olingo.server.api.uri.UriInfoResource;\n" +
            "import org.apache.olingo.server.api.uri.UriParameter;\n" +
            "import org.apache.olingo.server.api.uri.UriResource;\n" +
            "import org.apache.olingo.server.api.uri.UriResourceEntitySet;\n" +
            "\n" +
            "import java.util.List;\n" +
            "import java.util.Locale;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 13.05.17.\n" +
            " */\n" +
            "public class Util {\n" +
            "\n" +
            "    public static EdmEntitySet getEdmEntitySet(UriInfoResource uriInfo) throws ODataApplicationException {\n" +
            "\n" +
            "        List<UriResource> resourcePaths = uriInfo.getUriResourceParts();\n" +
            "        // To get the entity set we have to interpret all URI segments\n" +
            "        if (!(resourcePaths.get(0) instanceof UriResourceEntitySet)) {\n" +
            "            throw new ODataApplicationException(\"Invalid resource type for first segment.\",\n" +
            "                    HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);\n" +
            "        }\n" +
            "\n" +
            "        UriResourceEntitySet uriResource = (UriResourceEntitySet) resourcePaths.get(0);\n" +
            "\n" +
            "        return uriResource.getEntitySet();\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Поиск нужной сущности\n" +
            "     * @param edmEntityType\n" +
            "     * @param rt_entitySet\n" +
            "     * @param keyParams\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    public static Entity findEntity(EdmEntityType edmEntityType,\n" +
            "                                    EntityCollection rt_entitySet, List<UriParameter> keyParams)\n" +
            "            throws ODataApplicationException {\n" +
            "\n" +
            "        List<Entity> entityList = rt_entitySet.getEntities();\n" +
            "\n" +
            "        // loop over all entities in order to find that one that matches all keys in request\n" +
            "        // an example could be e.g. contacts(ContactID=1, CompanyID=1)\n" +
            "        for(Entity rt_entity : entityList){\n" +
            "            boolean foundEntity = entityMatchesAllKeys(edmEntityType, rt_entity, keyParams);\n" +
            "            if(foundEntity){\n" +
            "                return rt_entity;\n" +
            "            }\n" +
            "        }\n" +
            "\n" +
            "        return null;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Проверка на совпадение всех ключей сущности\n" +
            "     * @param edmEntityType\n" +
            "     * @param rt_entity\n" +
            "     * @param keyParams\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     */\n" +
            "    public static boolean entityMatchesAllKeys(EdmEntityType edmEntityType, Entity rt_entity,  List<UriParameter> keyParams)\n" +
            "            throws ODataApplicationException {\n" +
            "\n" +
            "        // loop over all keys\n" +
            "        for (final UriParameter key : keyParams) {\n" +
            "            // key\n" +
            "            String keyName = key.getName();\n" +
            "            String keyText = key.getText();\n" +
            "\n" +
            "            // Edm: we need this info for the comparison below\n" +
            "            EdmProperty edmKeyProperty = (EdmProperty )edmEntityType.getProperty(keyName);\n" +
            "            Boolean isNullable = edmKeyProperty.isNullable();\n" +
            "            Integer maxLength = edmKeyProperty.getMaxLength();\n" +
            "            Integer precision = edmKeyProperty.getPrecision();\n" +
            "            Boolean isUnicode = edmKeyProperty.isUnicode();\n" +
            "            Integer scale = edmKeyProperty.getScale();\n" +
            "            // get the EdmType in order to compare\n" +
            "            EdmType edmType = edmKeyProperty.getType();\n" +
            "            // Key properties must be instance of primitive type\n" +
            "            EdmPrimitiveType edmPrimitiveType = (EdmPrimitiveType)edmType;\n" +
            "\n" +
            "            // Runtime data: the value of the current entity\n" +
            "            Object valueObject = rt_entity.getProperty(keyName).getValue(); // null-check is done in FWK\n" +
            "\n" +
            "            // now need to compare the valueObject with the keyText String\n" +
            "            // this is done using the type.valueToString //\n" +
            "            String valueAsString = null;\n" +
            "            try {\n" +
            "                valueAsString = edmPrimitiveType.valueToString(valueObject, isNullable, maxLength,\n" +
            "                        precision, scale, isUnicode);\n" +
            "            } catch (EdmPrimitiveTypeException e) {\n" +
            "                throw new ODataApplicationException(\"Failed to retrieve String value\",\n" +
            "                        HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(),Locale.ENGLISH, e);\n" +
            "            }\n" +
            "\n" +
            "            if (valueAsString == null){\n" +
            "                return false;\n" +
            "            }\n" +
            "\n" +
            "            boolean matches = valueAsString.equals(keyText);\n" +
            "            if(!matches){\n" +
            "                // if any of the key properties is not found in the entity, we don't need to search further\n" +
            "                return false;\n" +
            "            }\n" +
            "        }\n" +
            "\n" +
            "        return true;\n" +
            "    }\n" +
            "}";

    public BuildUtilIFile() {
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return fileStructure;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
