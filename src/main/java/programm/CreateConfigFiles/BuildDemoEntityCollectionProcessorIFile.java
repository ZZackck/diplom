package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 11.05.17.
 */
public class BuildDemoEntityCollectionProcessorIFile implements IFilePattern {
    private String fileName = "DemoEntityCollectionProcessor.java";

    public BuildDemoEntityCollectionProcessorIFile() {
    }

    private String mainPattern = "package ODataUtils;\n" +
            "\n" +
            "import model.Utils.PrettyPrintClass;\n" +
            "import org.apache.olingo.commons.api.data.*;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntitySet;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntityType;\n" +
            "import org.apache.olingo.commons.api.edm.EdmProperty;\n" +
            "import org.apache.olingo.commons.api.format.ContentType;\n" +
            "import org.apache.olingo.commons.api.http.HttpHeader;\n" +
            "import org.apache.olingo.commons.api.http.HttpStatusCode;\n" +
            "import org.apache.olingo.server.api.*;\n" +
            "import org.apache.olingo.server.api.processor.EntityCollectionProcessor;\n" +
            "import org.apache.olingo.server.api.serializer.EntityCollectionSerializerOptions;\n" +
            "import org.apache.olingo.server.api.serializer.ODataSerializer;\n" +
            "import org.apache.olingo.server.api.serializer.SerializerResult;\n" +
            "import org.apache.olingo.server.api.uri.*;\n" +
            "import org.apache.olingo.server.api.uri.queryoption.*;\n" +
            "import org.apache.olingo.server.api.uri.queryoption.expression.Expression;\n" +
            "import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitException;\n" +
            "import org.apache.olingo.server.api.uri.queryoption.expression.Member;\n" +
            "\n" +
            "import java.io.*;\n" +
            "import java.util.*;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 06.05.17.\n" +
            " */\n" +
            "public class DemoEntityCollectionProcessor implements EntityCollectionProcessor {\n" +
            "    private OData odata;\n" +
            "    private ServiceMetadata serviceMetadata;\n" +
            "    private Storage storage;\n" +
            "\n" +
            "    public DemoEntityCollectionProcessor(Storage storage) {\n" +
            "        this.storage = storage;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Метод, вызываемый при GET запросе, чтение данных в бэкенд. Получение необходимых данных и отправка в OData\n" +
            "     * @param oDataRequest\n" +
            "     * @param oDataResponse\n" +
            "     * @param uriInfo\n" +
            "     * @param contentType\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void readEntityCollection(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo, ContentType contentType) throws ODataApplicationException, ODataLibraryException {\n" +
            "        // 1st we have retrieve the requested EntitySet from the uriInfo object (representation of the parsed service URI)\n" +
            "        List<UriResource> resourcePaths = uriInfo.getUriResourceParts();\n" +
            "        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0); // in our example, the first segment is the EntitySet\n" +
            "        EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();\n" +
            "\n" +
            "        // 2nd: fetch the data from backend for this requested EntitySetName it has to be delivered as EntitySet object\n" +
            "        EntityCollection entityCollection = storage.readEntitySetData(edmEntitySet);\n" +
            "\n" +
            "        List<Entity> entityList = entityCollection.getEntities();\n" +
            "        EntityCollection returnEntityCollection = new EntityCollection();\n" +
            "\n" +
            "        SelectOption selectOption = uriInfo.getSelectOption();\n" +
            "\n" +
            "        // handle $count: always return the original number of entities, without considering $top and $skip\n" +
            "        CountOption countOption = uriInfo.getCountOption();\n" +
            "        returnEntityCollection = countOption(returnEntityCollection, entityList, countOption);\n" +
            "\n" +
            "        // handle $skip\n" +
            "        SkipOption skipOption = uriInfo.getSkipOption();\n" +
            "        entityList = skipOption(entityList, skipOption);\n" +
            "\n" +
            "        // handle $top\n" +
            "        TopOption topOption = uriInfo.getTopOption();\n" +
            "        entityList = topOption(entityList, topOption);\n" +
            "\n" +
            "        orderByOption(entityList, uriInfo);\n" +
            "\n" +
            "        filterOption(entityList,uriInfo);\n" +
            "        // after applying the system query options, create the EntityCollection based on the reduced list\n" +
            "        for (Entity entity : entityList) {\n" +
            "            returnEntityCollection.getEntities().add(entity);\n" +
            "        }\n" +
            "\n" +
            "        // 3rd: create a serializer based on the requested format (json)\n" +
            "        ODataSerializer serializer = odata.createSerializer(contentType);\n" +
            "\n" +
            "        // 4th: Now serialize the content: transform from the EntitySet object to InputStream\n" +
            "        EdmEntityType edmEntityType = edmEntitySet.getEntityType();\n" +
            "\n" +
            "        String selectList = odata.createUriHelper().buildContextURLSelectList(edmEntityType,\n" +
            "                null, selectOption);\n" +
            "\n" +
            "        ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).selectList(selectList).build();\n" +
            "\n" +
            "        final String id = oDataRequest.getRawBaseUri() + \"/\" + edmEntitySet.getName();\n" +
            "        EntityCollectionSerializerOptions opts = EntityCollectionSerializerOptions.with().contextURL(contextUrl).count(countOption).select(selectOption).id(id).build();\n" +
            "        SerializerResult serializerResult = serializer.entityCollection(serviceMetadata, edmEntityType, returnEntityCollection, opts);\n" +
            "        InputStream serializedContent = serializerResult.getContent();\n" +
            "\n" +
            "        if (contentType.getSubtype().toLowerCase().equals(\"json\"))\n" +
            "            serializedContent = PrettyPrintClass.prettyPrint(serializedContent);\n" +
            "\n" +
            "        // Finally: configure the response object: set the body, headers and status code\n" +
            "        oDataResponse.setContent(serializedContent);\n" +
            "        oDataResponse.setStatusCode(HttpStatusCode.OK.getStatusCode());\n" +
            "        oDataResponse.setHeader(HttpHeader.CONTENT_TYPE, contentType.toContentTypeString());\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Инициализация\n" +
            "     * @param odata\n" +
            "     * @param serviceMetadata\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void init(OData odata, ServiceMetadata serviceMetadata) {\n" +
            "        this.odata = odata;\n" +
            "        this.serviceMetadata = serviceMetadata;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Операция COUNT\n" +
            "     * @param entityCollection\n" +
            "     * @param entityList\n" +
            "     * @param countOption\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    private EntityCollection countOption(EntityCollection entityCollection, List<Entity> entityList, CountOption countOption)\n" +
            "            throws ODataApplicationException, ODataLibraryException {\n" +
            "        if (countOption != null) {\n" +
            "            boolean isCount = countOption.getValue();\n" +
            "            if (isCount) {\n" +
            "                entityCollection.setCount(entityList.size());\n" +
            "            }\n" +
            "        }\n" +
            "        return entityCollection;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Операция SKIP\n" +
            "     * @param entityList\n" +
            "     * @param skipOption\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    private List<Entity> skipOption(List<Entity> entityList, SkipOption skipOption)\n" +
            "            throws ODataApplicationException, ODataLibraryException {\n" +
            "        if (skipOption != null) {\n" +
            "            int skipNumber = skipOption.getValue();\n" +
            "            if (skipNumber >= 0) {\n" +
            "                if(skipNumber <= entityList.size()) {\n" +
            "                    entityList = entityList.subList(skipNumber, entityList.size());\n" +
            "                } else {\n" +
            "                    // The client skipped all entities\n" +
            "                    entityList.clear();\n" +
            "                }\n" +
            "            } else {\n" +
            "                throw new ODataApplicationException(\"Invalid value for $skip\", HttpStatusCode.BAD_REQUEST.getStatusCode(),\n" +
            "                        Locale.ROOT);\n" +
            "            }\n" +
            "        }\n" +
            "        return entityList;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Операция TOP\n" +
            "     * @param entityList\n" +
            "     * @param topOption\n" +
            "     * @return\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    private List<Entity> topOption(List<Entity> entityList, TopOption topOption)\n" +
            "            throws ODataApplicationException, ODataLibraryException {\n" +
            "\n" +
            "        if (topOption != null) {\n" +
            "            int topNumber = topOption.getValue();\n" +
            "            if (topNumber >= 0) {\n" +
            "                if(topNumber <= entityList.size()) {\n" +
            "                    entityList = entityList.subList(0, topNumber);\n" +
            "                }  // else the client has requested more entities than available => return what we have\n" +
            "            } else {\n" +
            "                throw new ODataApplicationException(\"Invalid value for $top\", HttpStatusCode.BAD_REQUEST.getStatusCode(),\n" +
            "                        Locale.ROOT);\n" +
            "            }\n" +
            "        }\n" +
            "        return entityList;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Операция ORDERBY\n" +
            "     * @param entityList\n" +
            "     * @param uriInfo\n" +
            "     */\n" +
            "    private void orderByOption(List<Entity> entityList, UriInfo uriInfo) {\n" +
            "        OrderByOption orderByOption = uriInfo.getOrderByOption();\n" +
            "        if (orderByOption != null) {\n" +
            "            List<OrderByItem> orderItemList = orderByOption.getOrders();\n" +
            "            final OrderByItem orderByItem = orderItemList.get(0); // we support only one\n" +
            "            Expression expression = orderByItem.getExpression();\n" +
            "            if(expression instanceof Member){\n" +
            "                UriInfoResource resourcePath = ((Member)expression).getResourcePath();\n" +
            "                UriResource uriResource = resourcePath.getUriResourceParts().get(0);\n" +
            "                if (uriResource instanceof UriResourcePrimitiveProperty) {\n" +
            "                    EdmProperty edmProperty = ((UriResourcePrimitiveProperty)uriResource).getProperty();\n" +
            "                    final String sortPropertyName = edmProperty.getName();\n" +
            "\n" +
            "                    // do the sorting for the list of entities\n" +
            "                    Collections.sort(entityList, new Comparator<Entity>() {\n" +
            "\n" +
            "                        // delegate the sorting to native sorter of Integer and String\n" +
            "                        public int compare(Entity entity1, Entity entity2) {\n" +
            "                            int compareResult = 0;\n" +
            "\n" +
            "                            if(sortPropertyName.equals(\"ID\")){\n" +
            "                                Integer integer1 = (Integer) entity1.getProperty(sortPropertyName).getValue();\n" +
            "                                Integer integer2 = (Integer) entity2.getProperty(sortPropertyName).getValue();\n" +
            "\n" +
            "                                compareResult = integer1.compareTo(integer2);\n" +
            "                            }else{\n" +
            "                                String propertyValue1 = (String) entity1.getProperty(sortPropertyName).getValue();\n" +
            "                                String propertyValue2 = (String) entity2.getProperty(sortPropertyName).getValue();\n" +
            "\n" +
            "                                compareResult = propertyValue1.compareTo(propertyValue2);\n" +
            "                            }\n" +
            "\n" +
            "                            // if 'desc' is specified in the URI, change the order\n" +
            "                            if(orderByItem.isDescending()){\n" +
            "                                return - compareResult; // just reverse order\n" +
            "                            }\n" +
            "\n" +
            "                            return compareResult;\n" +
            "                        }\n" +
            "                    });\n" +
            "                }\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Операция FILTER\n" +
            "     * @param entityList\n" +
            "     * @param uriInfo\n" +
            "     */\n" +
            "    private void filterOption(List<Entity> entityList, UriInfo uriInfo) {\n" +
            "        FilterOption filterOption = uriInfo.getFilterOption();\n" +
            "        if(filterOption != null) {\n" +
            "            // Apply $filter system query option\n" +
            "            try {\n" +
            "                Iterator<Entity> entityIterator = entityList.iterator();\n" +
            "                // Evaluate the expression for each entity\n" +
            "                // If the expression is evaluated to \"true\", keep the entity otherwise remove it from the entityList\n" +
            "                while (entityIterator.hasNext()) {\n" +
            "                    // To evaluate the the expression, create an instance of the Filter Expression Visitor and pass the current entity to the constructor\n" +
            "                    Entity currentEntity = entityIterator.next();\n" +
            "                    Expression filterExpression = filterOption.getExpression();\n" +
            "                    FilterExpressionVisitor expressionVisitor = new FilterExpressionVisitor(currentEntity);\n" +
            "                    // Start evaluating the expression\n" +
            "                    Object visitorResult = filterExpression.accept(expressionVisitor);\n" +
            "                    // The result of the filter expression must be of type Edm.Boolean\n" +
            "                    if(visitorResult instanceof Boolean) {\n" +
            "                        if(!Boolean.TRUE.equals(visitorResult)) {\n" +
            "                            // The expression evaluated to false (or null), so we have to remove the currentEntity from entityList\n" +
            "                            entityIterator.remove();\n" +
            "                        }\n" +
            "                        } else {\n" +
            "                        throw new ODataApplicationException(\"A filter expression must evaulate to type Edm.Boolean\",\n" +
            "                                HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);\n" +
            "                        }\n" +
            "                    }\n" +
            "                } catch (ExpressionVisitException e) {\n" +
            "                try {\n" +
            "                    throw new ODataApplicationException(\"Exception in filter evaluation\",\n" +
            "                        HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ENGLISH);\n" +
            "                } catch (ODataApplicationException e1) {\n" +
            "                    e1.printStackTrace();\n" +
            "                }\n" +
            "            } catch (ODataApplicationException e) {\n" +
            "                e.printStackTrace();\n" +
            "            }\n" +
            "        }\n" +
            "    }\n" +
            "}";

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return mainPattern;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
