package programm.CreateConfigFiles;

import programm.Interfaces.IFilePattern;
import programm.Utilites.Constants;

/**
 * Created by kir on 14.05.17.
 */
public class BuildEntityProcessorIFile implements IFilePattern{
    private String fileName = "DemoEntityProcessor.java";

    private String fileStructure = "package ODataUtils;\n" +
            "\n" +
            "import model.Utils.PrettyPrintClass;\n" +
            "import org.apache.olingo.commons.api.data.ContextURL;\n" +
            "import org.apache.olingo.commons.api.data.Entity;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntitySet;\n" +
            "import org.apache.olingo.commons.api.edm.EdmEntityType;\n" +
            "import org.apache.olingo.commons.api.format.ContentType;\n" +
            "import org.apache.olingo.commons.api.http.HttpHeader;\n" +
            "import org.apache.olingo.commons.api.http.HttpMethod;\n" +
            "import org.apache.olingo.commons.api.http.HttpStatusCode;\n" +
            "import org.apache.olingo.server.api.*;\n" +
            "import org.apache.olingo.server.api.deserializer.DeserializerResult;\n" +
            "import org.apache.olingo.server.api.deserializer.ODataDeserializer;\n" +
            "import org.apache.olingo.server.api.processor.EntityProcessor;\n" +
            "import org.apache.olingo.server.api.serializer.EntitySerializerOptions;\n" +
            "import org.apache.olingo.server.api.serializer.ODataSerializer;\n" +
            "import org.apache.olingo.server.api.serializer.SerializerResult;\n" +
            "import org.apache.olingo.server.api.uri.UriInfo;\n" +
            "import org.apache.olingo.server.api.uri.UriParameter;\n" +
            "import org.apache.olingo.server.api.uri.UriResource;\n" +
            "import org.apache.olingo.server.api.uri.UriResourceEntitySet;\n" +
            "import org.apache.olingo.server.api.uri.queryoption.SelectOption;\n" +
            "\n" +
            "import java.io.InputStream;\n" +
            "import java.util.List;\n" +
            "\n" +
            "/**\n" +
            " * Created by kir on 13.05.17.\n" +
            " */\n" +
            "public class DemoEntityProcessor implements EntityProcessor {\n" +
            "\n" +
            "    private OData odata;\n" +
            "    private ServiceMetadata serviceMetadata;\n" +
            "    private Storage storage;\n" +
            "\n" +
            "    /**\n" +
            "     * Конструктор\n" +
            "     * @param storage\n" +
            "     */\n" +
            "    public DemoEntityProcessor(Storage storage) {\n" +
            "        this.storage = storage;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Инициализация\n" +
            "     * @param odata\n" +
            "     * @param serviceMetadata\n" +
            "     */\n" +
            "    public void init(OData odata, ServiceMetadata serviceMetadata) {\n" +
            "        this.odata = odata;\n" +
            "        this.serviceMetadata = serviceMetadata;\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Метод такой же как и readEntityCollection в классе DemoEntityCollectionProcessor, но относится к одной сущности а не к коллекции в целом, HTTP-запрос GET\n" +
            "     * @param oDataRequest\n" +
            "     * @param oDataResponse\n" +
            "     * @param uriInfo\n" +
            "     * @param contentType\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void readEntity(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo, ContentType contentType) throws ODataApplicationException, ODataLibraryException {\n" +
            "        List<UriResource> resourcePaths = uriInfo.getUriResourceParts();\n" +
            "        // Note: only in our example we can assume that the first segment is the EntitySet\n" +
            "        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0);\n" +
            "        EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();\n" +
            "\n" +
            "        // 2. retrieve the data from backend\n" +
            "        List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();\n" +
            "        Entity entity = storage.readEntityData(edmEntitySet, keyPredicates);\n" +
            "\n" +
            "        SelectOption selectOption = uriInfo.getSelectOption();\n" +
            "\n" +
            "        // 3. serialize\n" +
            "        EdmEntityType entityType = edmEntitySet.getEntityType();\n" +
            "\n" +
            "        String selectList = odata.createUriHelper().buildContextURLSelectList(entityType, null, selectOption);\n" +
            "\n" +
            "        ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).selectList(selectList).build();\n" +
            "        // expand and select currently not supported\n" +
            "        EntitySerializerOptions options = EntitySerializerOptions.with().contextURL(contextUrl).select(selectOption).build();\n" +
            "\n" +
            "        ODataSerializer serializer = odata.createSerializer(contentType);\n" +
            "        SerializerResult serializerResult = serializer.entity(serviceMetadata, entityType, entity, options);\n" +
            "        InputStream entityStream = serializerResult.getContent();\n" +
            "\n" +
            "        if (contentType.getSubtype().toLowerCase().equals(\"json\"))\n" +
            "            entityStream = PrettyPrintClass.prettyPrint(entityStream);\n" +
            "\n" +
            "        //4. configure the response object\n" +
            "        oDataResponse.setContent(entityStream);\n" +
            "        oDataResponse.setStatusCode(HttpStatusCode.OK.getStatusCode());\n" +
            "        oDataResponse.setHeader(HttpHeader.CONTENT_TYPE, contentType.toContentTypeString());\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Создание сущности\n" +
            "     * @param oDataRequest\n" +
            "     * @param oDataResponse\n" +
            "     * @param uriInfo\n" +
            "     * @param contentType\n" +
            "     * @param contentType1\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void createEntity(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo, ContentType contentType, ContentType contentType1) throws ODataApplicationException, ODataLibraryException {\n" +
            "        EdmEntitySet edmEntitySet = Util.getEdmEntitySet(uriInfo);\n" +
            "        EdmEntityType edmEntityType = edmEntitySet.getEntityType();\n" +
            "\n" +
            "        // 2. create the data in backend\n" +
            "         // 2.1. retrieve the payload from the POST request for the entity to create and deserialize it\n" +
            "        InputStream requestInputStream = oDataRequest.getBody();\n" +
            "        ODataDeserializer deserializer = this.odata.createDeserializer(contentType);\n" +
            "        DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);\n" +
            "        Entity requestEntity = result.getEntity();\n" +
            "         // 2.2 do the creation in backend, which returns the newly created entity\n" +
            "        Entity createdEntity = storage.createEntityData(edmEntitySet, requestEntity);\n" +
            "\n" +
            "        // 3. serialize the response (we have to return the created entity)\n" +
            "        ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).build();\n" +
            "        EntitySerializerOptions options = EntitySerializerOptions.with().contextURL(contextUrl).build(); // expand and select currently not supported\n" +
            "\n" +
            "        ODataSerializer serializer = this.odata.createSerializer(contentType);\n" +
            "        SerializerResult serializedResponse = serializer.entity(serviceMetadata, edmEntityType, createdEntity, options);\n" +
            "\n" +
            "        //4. configure the response object\n" +
            "        oDataResponse.setContent(serializedResponse.getContent());\n" +
            "        oDataResponse.setStatusCode(HttpStatusCode.CREATED.getStatusCode());\n" +
            "        oDataResponse.setHeader(HttpHeader.CONTENT_TYPE, contentType.toContentTypeString());\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Обновление сущности: HTTP-запросы PUT, PATCH\n" +
            "     * @param oDataRequest\n" +
            "     * @param oDataResponse\n" +
            "     * @param uriInfo\n" +
            "     * @param contentType\n" +
            "     * @param contentType1\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void updateEntity(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo, ContentType contentType, ContentType contentType1) throws ODataApplicationException, ODataLibraryException {\n" +
            "        // 1. Retrieve the entity set which belongs to the requested entity\n" +
            "        List<UriResource> resourcePaths = uriInfo.getUriResourceParts();\n" +
            "        // Note: only in our example we can assume that the first segment is the EntitySet\n" +
            "        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0);\n" +
            "        EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();\n" +
            "        EdmEntityType edmEntityType = edmEntitySet.getEntityType();\n" +
            "\n" +
            "        // 2. update the data in backend\n" +
            "        // 2.1. retrieve the payload from the PUT request for the entity to be updated\n" +
            "        InputStream requestInputStream = oDataRequest.getBody();\n" +
            "        ODataDeserializer deserializer = this.odata.createDeserializer(contentType);\n" +
            "        DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);\n" +
            "        Entity requestEntity = result.getEntity();\n" +
            "        // 2.2 do the modification in backend\n" +
            "        List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();\n" +
            "        // Note that this updateEntity()-method is invoked for both PUT or PATCH operations\n" +
            "        HttpMethod httpMethod = oDataRequest.getMethod();\n" +
            "        storage.updateEntityData(edmEntityType, keyPredicates, requestEntity, httpMethod);\n" +
            "\n" +
            "        //3. configure the response object\n" +
            "        oDataResponse.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());\n" +
            "    }\n" +
            "\n" +
            "    /**\n" +
            "     * Удаление сущности, HTTP-запрос DELETE\n" +
            "     * @param oDataRequest\n" +
            "     * @param oDataResponse\n" +
            "     * @param uriInfo\n" +
            "     * @throws ODataApplicationException\n" +
            "     * @throws ODataLibraryException\n" +
            "     */\n" +
            "    @Override\n" +
            "    public void deleteEntity(ODataRequest oDataRequest, ODataResponse oDataResponse, UriInfo uriInfo) throws ODataApplicationException, ODataLibraryException {\n" +
            "        // 1. Retrieve the entity set which belongs to the requested entity\n" +
            "        List<UriResource> resourcePaths = uriInfo.getUriResourceParts();\n" +
            "        // Note: only in our example we can assume that the first segment is the EntitySet\n" +
            "        UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0);\n" +
            "        EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();\n" +
            "\n" +
            "        // 2. delete the data in backend\n" +
            "        List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();\n" +
            "        storage.deleteEntityData(edmEntitySet, keyPredicates);\n" +
            "\n" +
            "        //3. configure the response object\n" +
            "        oDataResponse.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());\n" +
            "    }\n" +
            "}\n";

    public BuildEntityProcessorIFile() {
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public String getFileStructure() {
        return fileStructure;
    }

    @Override
    public String getFileDestination() {
        return Constants.ODATAUTILS;
    }
}
